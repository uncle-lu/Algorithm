#include<cstdio>//uncle-lu
#include<cstring>
#include<bitset>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void add_edge(int, int);
void init(void);
void DFS(int);

struct node{
	int v,next;
};
node edge[100010];
int head[310],cnt,col[310];
int n,m,q;
std::bitset<303>G[303];
std::bitset<310>visit;

void init()
{
	memset(head,-1,sizeof(head));
	memset(col,0,sizeof(col));
	cnt=0;
	return ;
}

void add_edge(int x, int y)
{
	edge[++cnt].v=y;
	edge[cnt].next=head[x];
	head[x] = cnt;
	return ;
}

void DFS(int x)
{
	visit[x] = 1;
	G[x][x] = 1;
	if(col[x])return ;

	for(int i=head[x];~i;i=edge[i].next)
	{
		if(!visit[edge[i].v])DFS(edge[i].v);
		if(!col[edge[i].v])G[x] |= G[edge[i].v];
	}
	return ;
}

int main()
{
	int a,b;
	while(~scanf("%d%d%d",&n,&m,&q))
	{
		init();
		for(int i=1;i<=m;++i)
		{
			read(a);read(b);
			add_edge(a,b);
		}

		for(int i=1;i<=q;++i)
		{
			read(a);
			col[a] ^= 1;

			for(int j=1;j<=n;++j)
				G[j].reset();
			visit.reset();

			long long int ans=0;

			for(int j=1;j<=n;++j)
			{
				if(col[j])continue;
				if(!visit[j])DFS(j);
				ans += G[j].count() - 1;
			}

			printf("%lld\n",ans);
		}
	}
	return 0;
}
