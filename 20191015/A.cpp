#include<cstdio>//uncle-lu
#include<algorithm>

template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int n,m;

int main()
{
	while(~scanf("%lld%lld",&n,&m))
	{
		long long int ans = 0;

		for(int i=1; i <= std::min(n,2016LL); ++i)
			for(int j=1; j <= std::min(m,2016LL); ++j)
				if( i * j % 2016 == 0)
					ans += (long long int)( (n-i) / 2016 + 1) * ((m-j) / 2016 + 1);

		printf("%lld\n",ans);
	}

	return 0;
}
