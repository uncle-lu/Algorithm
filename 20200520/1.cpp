#include<cstdio>
#include<cstring>
#include<algorithm>

struct node{
	char cls[20];
	char comp[20];
	int time;
	int count;
	float cost;
	friend bool operator<(const node a,const node b)
	{
		if(strcmp(a.comp, b.comp) < 0)return true;
		else if(strcmp(a.comp, b.comp) > 0)return false;

		if(a.time < b.time)return true;
		else if(a.time > b.time)return false;

		return true;
	}
};

node list[200];

int main()
{
	int n, begin, end;
	scanf("%d", &n);

	for(int i=1; i<=n; ++i)
		scanf("%s %s %d %d %f", list[i].cls, list[i].comp, &list[i].time, &list[i].count, &list[i].cost);

	scanf("%d %d", &begin, &end);

	std::sort(list+1, list+1+n);

	char now[20];
	float cnt = 0, ans = 0;
	for(int i=1; i<=n+1; ++i)
	{
		if(strcmp(list[i].comp, now)!=0)
		{
			if(i!=1)printf("%-16s%-.2f\n", now, cnt);
			printf("\n");
			strcpy(now, list[i].comp);
			ans += cnt;
			cnt = 0;
		}

		if( list[i].time < begin || list[i].time > end )continue;

		char t[20];int pt=0, temp = list[i].time;
		while(temp)
		{
			t[pt] = temp%10 + '0';
			temp/=10;
			pt++;
		}
		pt--;
		for(int j=0; j<pt/2; ++j)
			std::swap(t[j], t[pt-j]);
		t[pt+1] = '\0';

		printf("%-16s%s\t%-10s%d  %-11.2f%-.2f\n", list[i].cls, list[i].comp, t, list[i].count, list[i].cost, list[i].count*list[i].cost);
		
		cnt += list[i].count * list[i].cost;
	}

	printf("%-36.2f\n", ans);
	return 0;
}
