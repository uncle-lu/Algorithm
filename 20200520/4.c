#include<stdio.h>

struct time_T{
	int h, m, s;
};

struct time_T time_pass(struct time_T start, int passed_sec)
{
	struct time_T now;
	now = start;
	now.s += passed_sec;
	if(now.s >= 60)
	{
		now.m += now.s/60;
		now.s %= 60;
	}
	if(now.m >= 60)
	{
		now.h += now.m/60;
		now.m %=60;
	}
	if(now.h >= 24)
	{
		now.h %=24;
	}

	return now;
}

int main()
{
	struct time_T now;
	int sec;
	scanf("%d:%d:%d", &now.h, &now.m, &now.s);
	scanf("%d", &sec);

	now = time_pass(now, sec);

	if( now.h < 10)
	{
		printf("0%d:",now.h);
	}
	else
	{
		printf("%d:",now.h);
	}

	if( now.m < 10 )
	{
		printf("0%d:", now.m);
	}
	else
	{
		printf("%d:",now.m);
	}

	if(now.s < 10)
	{
		printf("0%d", now.s);
	}
	else
	{
		printf("%d",now.s);
	}
	return 0;
}
