#include<cstdio>//uncle-lu
#include<algorithm>
#include<cstring>
using namespace std;
int one[5][5],two[5][5],cnt;
int forces[17][17];
int x_turn[4]={1,-1,0,0},y_turn[4]={0,0,1,-1};
int visit[5][5],ans=214748;
int visit_1[20];
struct node{
	int x,y,step;
}queue[1001];
void BFS(int x_1,int y_1,int number)
{
	memset(visit,0,sizeof(visit));
	int head=1,last=1;
	node first;
	first.x=x_1;first.y=y_1;first.step=0;
	queue[1]=first;visit[x_1][y_1]=1;
	while(head<=last)
	{
		node news=queue[head];
		for(int i=0;i<4;++i)
		{
			int x=news.x+x_turn[i],y=news.y+y_turn[i];
			if(x>0&&x<=4&&y>0&&y<=4&&(!visit[x][y]))
			{
				node zz;
				zz.x=x;zz.y=y;zz.step=news.step+1;
				if(two[x][y]){
					forces[number][two[x][y]]=zz.step;
				}
				queue[++last]=zz;
				visit[x][y]=1;
			}
		}
		++head;
	}
	return ;
}
int lie[20];
void DFS(int x)
{
	if(x>cnt)
	{
		int ans_1=0;
		for(int i=1;i<=cnt;++i)
		{
			ans_1+=forces[i][lie[i]];
		}
		ans=min(ans,ans_1);
		return ;
	}
	for(int i=1;i<=cnt;i++)
	{
		if(visit_1[i])continue;
		visit_1[i]=1;
		lie[x]=i;
		DFS(x+1);
		visit_1[i]=0;
		lie[x]=0;
	}
	return ;
}
int main()
{
	for(int i=1;i<=4;++i)
		for(int j=1;j<=4;++j)
			scanf("%1d",&one[i][j]);
	for(int i=1;i<=4;++i)
		for(int j=1;j<=4;++j)
		{
			scanf("%1d",&two[i][j]);
			if(two[i][j]==1&&two[i][j]==one[i][j])
				one[i][j]=two[i][j]=0;
			else if(two[i][j]==1){
				++cnt;
				two[i][j]=cnt;
			}
		}
	int cnt_1=0;
	for(int i=1;i<=4;i++)
		for(int j=1;j<=4;++j)
		{
			if(one[i][j])
			{
				++cnt_1;
				BFS(i,j,cnt_1);
			}
		}
	DFS(1);
	printf("%d",ans);
	return 0;
}
