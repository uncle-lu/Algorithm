#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int N = 1e6+10;

struct node{
	int l, r, sit, ans;
};
int n, m;
int line[N], Tree[N], next[N];
node list[N];

bool cmp1(node a, node b)
{
	return a.r < b.r;
}

bool cmp2(node a, node b)
{
	return a.sit < b.sit;
}

int lowbit(int x) { return x & (-x); }

void update(int sit, int x)
{
	while(sit <= n)
	{
		Tree[sit] += x;
		sit += lowbit(sit);
	}
	return ;
}

int getsum(int sit)
{
	int sum = 0;
	while(sit > 0)
	{
		sum += Tree[sit];
		sit -= lowbit(sit);
	}
	return sum;
}

int main()
{
	read(n);
	for(int i=1; i<=n; ++i)
		read(line[i]);
	read(m);
	for(int i=1; i<=m; ++i)
	{
		read(list[i].l);read(list[i].r); list[i].sit = i;
	}

	std::sort(list+1, list+1+m, cmp1);

	int last=1;

	for(int i=1; i<=m; ++i)
	{
		for(int j=last; j<=list[i].r; ++j)
		{
			if(next[line[j]])
				update(next[line[j]], -1);
			update(j, 1);
			next[line[j]] = j;
		}

		last = list[i].r + 1;
		list[i].ans = getsum(list[i].r) - getsum(list[i].l-1);
	}

	std::sort(list+1, list+1+m, cmp2);

	for (int i = 1; i <= m; i++) 
	{
		printf("%d\n",list[i].ans);
	}

	return 0;
}
