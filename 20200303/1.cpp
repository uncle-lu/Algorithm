/*
 * Author: uncle-lu 21190603
 * Mail: uncle-lu@qq.com
 * Blog: uncle-lu.org
 * File name: 2-1.cpp
 * Description: C++ course assignments.
 */
#include<iostream>

class Time{
	private:
		int hour, minute, second;
	public:
		Time():hour(0),minute(0),second(0){}
		Time(int h, int m, int s):hour(h),minute(m),second(s){}

		void add()
		{
			second++;
			if(second>=60)
			{
				minute += second/60;
				second %= 60;
			}
			if(minute>=60)
			{
				hour += minute/60;
				minute %= 60;
			}
			return ;
		}

		void sub(const Time& temp)
		{
			hour += temp.hour;
			minute += temp.minute;
			second += temp.second;

			if(second>=60)
			{
				minute += second/60;
				second %= 60;
			}
			if(minute>=60)
			{
				hour += minute/60;
				minute %= 60;
			}
			return ;
		}

		void print()
		{
			std::cout << "hour:" << hour << " minute:" << minute << " second:" << second << std::endl;
			return ;
		}
};

int main()
{
	Time t_1(2, 4, 3) ,t_2(1, 5, 18);

	t_1.add();
	t_1.print();

	t_1.sub(t_2);
	t_1.print();

	return 0;
}
