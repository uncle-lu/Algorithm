#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 2*1e5+10;

int n, p, k;
int line[INF];
int sum[INF];

int main()
{
	int T;
	read(T);
	for(int t=1; t<=T; ++t)
	{
		read(n);read(p);read(k);
		for(int i=1; i<=n; ++i)
			read(line[i]);
		
		std::sort(line+1, line+1+n);
		for(int i=1; i<=n; ++i)
			sum[i] = sum[i-1] + line[i];
		
		for(int i=2; i<=n; ++i)
		{
			sum[i] = sum[i] - line[i-1];
		}

		int ans = 0;
		for(int i=1; i<=n; ++i)
			if(sum[i] <= p)
				ans = i;

		printf("%d\n", ans);
	}
	return 0;
}
