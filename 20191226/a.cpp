#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int main()
{
	int T, x, y, s, r;
	read(T);
	for(int t=1; t<=T; ++t)
	{
		read(x);read(y);read(s);read(r);
		int ans = 0;
		if(x > y)
			std::swap(x, y);

		if(s <= x)
		{
			if(s+r <= x)
				ans = y - x ;
			else if ( s+r <= y)
				ans = y - (s+r) ;
			else
				ans = 0;
		}
		else if(s <= y)
		{
			if(s-r > x)
				ans += (s-r) - x ;
			if(s+r < y)
				ans += y - (s+r) ;
		}
		else if(s > y)
		{
			if(s-r <= x)
				ans = 0;
			else if(s-r < y)
				ans = (s-r) - x ;
			else 
				ans = y - x ;
		}

		printf("%d\n",ans);
	}
	
	return 0;
}
