#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct li{

	struct node{
		int front, next, val;
	};
	node l[10010];
	int top;

	li(){
		for(int i=0;i<10010;++i)
			l[i].front = -1, l[i].next = -1, l[i].val = -1;

		l[0].next = 0;
		l[0].front = 0;
		top = 0;
	}

	void add(int x, int val)
	{
		top++;

		l[top] = (node){x, l[x].next, val};
		l[x].next = top;
		l[ l[top].next ].front = top;

		return ;
	}

	void del(int val)
	{
		for(int i = l[0].next; i; i = l[i].next)
		{
			if(l[i].val == val)
			{
				l[ l[i].front ].next = l[i].next;
				l[ l[i].next ].front = l[i].front;
			}
		}
		return ;
	}

	void add_from_head(int x, int val)
	{
		int sit = 0;
		int i = 0;
		while(i < x-1)
		{
			i++;
			sit = l[sit].next;
		}
		add(sit, val);
		return ;
	}

	void add_from_last(int x, int val)
	{
		int sit = l[0].front;
		int i=1;
		while(sit && i < x)
		{
			i++;
			sit = l[sit].front;
		}
		add(sit, val);
		return ;
	}

	void print()
	{
		for(int i=l[0].next; i; i = l[i].next)
			printf("%d ",l[i].val);

		printf("\n");
		return ;
	}
};

li lis; 

int main()
{
	int n,temp;
	read(n);
	for(int i=1;i<=n;++i)
	{
		read(temp);
		if(temp == 1)
		{
			int a, b;
			read(a);read(b);
			lis.add_from_head(a, b);
		}
		else if(temp == 2)
		{
			int a, b;
			read(a);read(b);
			lis.add_from_last(a, b);
		}
		else if(temp == 3)
		{
			int a;
			read(a);
			lis.add(0, a);
		}
		else if(temp == 4)
		{
			int a;
			read(a);
			lis.del(a);
		}
		else if(temp == 5)
			lis.print();
	}

	return 0;
}
