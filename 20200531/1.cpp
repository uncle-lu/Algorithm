#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int N = 1e5*3;

void Build_Tree(int, int, int);
void Add_Tree(int, int, int, int, int, int);
void Mul_Tree(int, int, int, int, int, int);
void push_down(int, int, int);
int Find_Tree(int, int, int, int, int);

int n, m, mod;
int Tree[N], Lazy_mul[N], Lazy_add[N], line[N];

void push_down(int step, int x, int y)
{
	if(Lazy_mul[step]!=1 || Lazy_add[step])
	{
		Tree[step<<1] = (Tree[step<<1] * Lazy_mul[step] + Lazy_add[step]) % mod;
		Tree[step<<1|1] = (Tree[step<<1|1] * Lazy_mul[step] + Lazy_add[step]) % mod;
		Lazy_add[step<<1] = (Lazy_add[step<<1] * Lazy_mul[step] + Lazy_add[step]) % mod;
		Lazy_add[step<<1|1] = (Lazy_add[step<<1|1] * Lazy_mul[step] + Lazy_add[step]) % mod;
		Lazy_mul[step<<1] = (Lazy_mul[step<<1] * Lazy_mul[step]) % mod;
		Lazy_mul[step<<1|1] = (Lazy_mul[step<<1|1] * Lazy_mul[step]) % mod;
		Lazy_mul[step] = 1;
		Lazy_add[step] = 0;
	}
	return;
}

void Build_Tree(int step, int x, int y)
{
	if(x == y)
	{
		Tree[step] = line[x];
		return ;
	}
	int mid = (x+y)>>1;
	Build_Tree(step<<1, x, mid);
	Build_Tree(step<<1|1, mid+1, y);
	Tree[step] = (Tree[step<<1] + Tree[step<<1|1]) % mod;
	return ;
}

void Add_Tree(int step, int l, int r, int x, int y, int val)
{

}

void Mul_Tree(int step, int l, int r, int x, int y, int val)
{

}

int main()
{
	read(n);read(m);read(mod);
	for (int i = 1; i <= n; i++)
		read(line[i]);
	Build_Tree(1,1,n);

	int temp;
	for (int i = 1; i <= m; i++)
	{
		read(temp);
		if(temp == 1)
		{

		}
		else if(temp == 2)
		{

		}
		else if(temp == 3)
		{

		}
		else if(temp == 4)
		{

		}
	}

	return 0;
}
