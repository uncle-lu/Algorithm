// INFO BEGIN
//
// User = 201911508158(卢翼) 
// Group = C/C++ 
// Problem = 化学方程式 
// Language = CPP11 
// SubmitTime = 2019-12-15 16:40:43 
//
// INFO END

#include <iostream> //uncle-lu
#include <string>
#include <map>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;bool f=false;char ch=getchar();
	while(ch<'0'||ch>'9'){f|=(ch=='-'); ch=getchar();}
	while(ch<='9' && ch>='0'){ x = (x<<1) + (x<<3) + (ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void solution(void);
void group(std::map<std::string, int>* , std::string*);
void word(std::map<std::string, int>*, std::string*);
bool check(std::map<std::string, int>*, std::map<std::string, int>*);

void solution()
{
	std::map<std::string, int>qu1;
	std::map<std::string, int>qu2;
	std::string a, b, line;
	std::cin >> line;
	
	bool flag = false;
	for(auto c : line)
	{
		if(c == '=')
		{
			flag = true;
			continue;
		}
		if(!flag)
			a += c;
		else
			b += c;
	}
	
	group(&qu1, &a);
	group(&qu2, &b);
	
	if(check(&qu1, &qu2))
		printf("Y\n");
	else
		printf("N\n");
	
	return ;
}

bool check(std::map<std::string, int> *qu1, std::map<std::string, int> *qu2)
{
	for(std::map<std::string, int>::iterator i = qu1->begin(); i!=qu1->end(); ++i)
	{
		//std::cout << i->first << ' ' << i->second << ' ' << (*qu2)[i->first] << std::endl;
		if((*qu2)[i->first] != i->second)
			return false;
	}
	for(std::map<std::string, int>::iterator i = qu2->begin(); i!=qu2->end(); ++i)
	{
		if((*qu1)[i->first] != i->second)
			return false;
	}
	return true;
}

void group(std::map<std::string, int> *m, std::string * line)
{
	std::string now ;
	for(std::string::iterator i = line->begin(); i != line->end(); ++i)
	{
		if(*i == '+')
		{
			word(m,&now);
			now.clear();
			continue;
		}
		now += *i;
	}
	word(m,&now);
	return ;
}

void word(std::map<std::string, int> *m, std::string *line)
{
	int k=0;
	std::string::iterator i = line->begin();
	while(i!=line->end() && *i <= '9' && *i >= '0')
	{
		k = k * 10 + *i - '0';
		i++;
	}
	if(!k)k=1;
	
	int c = 0;
	std::string now;
	now = now + *i;
	i++;
	while(i!=line->end())
	{
		if(*i<='9' && *i>='0')
				c = c*10 + *i - '0';
		if(*i<='z' && *i >= 'a')
			now = now + *i;
		if(*i<='Z' && *i>= 'A')
		{
			if(m->find(now) == m->end())
			{
				(*m)[now] = 0;
				(*m)[now] += k*(c ? c : 1);
			}
			else
				(*m)[now] += k*(c ? c : 1);
				
			now.clear();
			c = 0;
			now = now + *i;
		}
		
		i++;
	}
	
	if(m->find(now) == m->end())
	{
		(*m)[now] = 0;
		(*m)[now] += k*(c ? c : 1);
	}
	else
		(*m)[now] += k*(c ? c : 1);
		
	return ;
}

int main()
{
	//freopen("in","r",stdin);
	int T;
	read(T);
	while(T--)
		solution();
		
	//fclose(stdin);
	return 0;
}