// INFO BEGIN
//
// User = 201911508158(¬��) 
// Group = C/C++ 
// Problem = ����վѡַ 
// Language = CPP11 
// SubmitTime = 2019-12-15 14:05:08 
//
// INFO END

#include <cstdio> //uncle-lu
#include <cmath>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;bool f=false;char ch=getchar();
	while(ch<'0'||ch>'9'){f|=(ch=='-'); ch=getchar();}
	while(ch<='9' && ch>='0'){ x = (x<<1) + (x<<3) + (ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 1010;

struct node{
	int x, y;
};
node line[INF];
int n;
int count[5];

int main()
{
	read(n);
	for(int i=1; i<=n; ++i)
	{
		read(line[i].x);
		read(line[i].y);
	}
	
	for(int i=1; i<=n; ++i)
	{
		int c = 0, flag = 0;
		for(int j=1; j<=n; ++j)
		{
			if(i == j) continue;
			
			if(line[j].x == line[i].x)
			{
				if(abs(line[j].y - line[i].y) == 1)
					flag++;
			}
			else if(line[j].y == line[i].y)
			{
				if(abs(line[j].x - line[i].x) == 1)
					flag++;
			}
			else
			{
				if(abs(line[j].x - line[i].x) == 1 && abs(line[j].y - line[i].y) == 1)
					c++;
			}
		}
		if(flag == 4)
		{
			count[c] ++;
		}
	}
	
	for(int i=0; i<=4; ++i)
		printf("%d\n", count[i]);
	
	return 0;
}