// INFO BEGIN
//
// User = 201911508158(¬��) 
// Group = C/C++ 
// Problem = ���� 
// Language = CPP11 
// SubmitTime = 2019-12-15 13:43:39 
//
// INFO END

#include <cstdio> //uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;bool f=false;char ch=getchar();
	while(ch<'0'||ch>'9'){f|=(ch=='-'); ch=getchar();}
	while(ch<='9' && ch>='0'){ x = (x<<1) + (x<<3) + (ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}
const int INF = 1000;

bool check(int);

int count[4];

bool check(int x)
{
	if(x % 7 == 0)
		return true;
	
	while(x)
	{
		if(x % 10 == 7)
			return true;
		x /= 10;
	}	
	
	return false;
}

int main()
{
	int n, temp = 0, count_all = 1;
	read(n);
	
	for (int i=1;; ++i)
	{
		if(count_all > n)
			break;
		else
			count_all ++;
			
		temp %= 4;
		if( check(i) )
		{
			//printf("i = %d temp = %d\n",i,temp);
			count[temp]++;
			count_all --;
		}
		temp++;
	}
	
	for (int i=0; i<4; ++i)
		printf("%d\n", count[i]);
		
	return 0;
}