#include<cstdio>//uncle-lu
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int v, next, val;
};
node edge[];
int n, ml, dl;


int main()
{
	read(n); read(ml); read(dl);

	int x, y, val;
	for(int i=1; i<=ml; i++)
	{
		read(x);read(y);read(val);
		add_edge(x, y, val);
	}
	for(int i=1; i<=dl; ++i)
	{
		read(x);read(y);read(val);
		add_edge(x, y, val);
	}

	return 0;
}
