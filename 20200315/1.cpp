#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m, q;
int h[1010],l[1010];
int ht[1010],lt[1010];

int main()
{
	read(n);read(m);read(q);
	for(int i=1; i<=q; i++)
	{
		int temp, sit, col;
		read(temp);read(sit);read(col);
		if(temp == 1)
		{
			h[sit] = col;
			ht[sit] = i;
		}
		else
		{
			l[sit] = col;
			lt[sit] = i;
		}
	}

	for(int i=1; i<=n; ++i)
	{
		for(int j=1; j<=m; ++j)
		{
			if(ht[i] > lt[j])
				printf("%d ",h[i]);
			else
				printf("%d ",l[j]);
		}
		printf("\n");
	}

	return 0;
}
