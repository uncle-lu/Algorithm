#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void solution(void);

int n, left, right;
int line[200010];
int sit[200010];

void solution()
{
	left = sit[1];
	right = sit[1];
	for(int i=1; i<=n ; ++i)
	{
		left = std::min(left, sit[i]);
		right = std::max(right, sit[i]);
		if(right - left + 1 == i)
			printf("1");
		else
			printf("0");
	}
	printf("\n");
	return ;
}

int main()
{
	int T;
	read(T);
	for(int t=1;t<=T;++t)
	{
		read(n);
		for(int i=1;i<=n;++i)
		{
			read(line[i]);
			sit[line[i]] = i;
		}
		solution();
	}
	return 0;
}
