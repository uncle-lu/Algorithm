#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void solution(void);
bool check();

int n;
char line[100010];

bool check()
{
	int len = strlen(line);
	for(int i=0; i<len-1;++i)
		if(line[i] == line[i+1] && line[i] != '?')return true;
	return false;
}

void solution()
{
	if(check())
	{
		printf("-1\n");
		return ;
	}
	int len = strlen(line);
	if(len == 1 && line[0] == '?')
	{
		line[0] = 'a';
	}
	if(line[0]=='?' && len > 1)
	{
		if(line[1] == 'a')
			line[0] = 'b';
		else if(line[1] == 'b')
			line[0] = 'c';
		else 
			line[0] = 'a';
	}
	for(int i=1;i<len;++i)
	{
		if(line[i]=='?')
		{
			if(i<len-1 && line[i+1]!='?')
			{
				if(line[i-1] == 'a' && line[i+1] == 'a')
					line[i] = 'b';
				else if(line[i-1] == 'a' && line[i+1] == 'b')
					line[i] = 'c';
				else if(line[i-1] == 'a' && line[i+1] == 'c')
					line[i] = 'b';
				else if(line[i-1] == 'b' && line[i+1] == 'b')
					line[i] = 'a';
				else if(line[i-1] == 'b' && line[i+1] == 'a')
					line[i] = 'c';
				else if(line[i-1] == 'b' && line[i+1] == 'c')
					line[i] = 'a';
				else if(line[i-1] == 'c' && line[i+1] == 'c')
					line[i] = 'a';
				else if(line[i-1] == 'c' && line[i+1] == 'a')
					line[i] = 'b';
				else 
					line[i] = 'a';
			}
			else
			{
				if(line[i-1] == 'a')
					line[i] = 'b';
				else if(line[i-1] == 'b')
					line[i] = 'c';
				else 
					line[i] = 'a';
			}
		}
	}
	printf("%s\n",line);
	return ;
}

int main()
{
	read(n);
	for(int i=1;i<=n;++i)
	{
		memset(line,0,sizeof(line));
		scanf("%s",line);
		solution();
	}
	return 0;
}
