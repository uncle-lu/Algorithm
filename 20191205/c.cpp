#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
int line[400010];
int count[400010];

int main()
{
	int T;
	read(T);
	while(T--)
	{
		read(n);
		for(int i=1;i<=n;++i)
			read(line[i]);

		if(n < 6)
		{
			printf("0 0 0\n");
			continue;
		}

		memset(count,0,sizeof(count));
		int temp = 0; 
		line[0] = -1;

		for(int i=1; i<=n ; ++i)
		{
			if(line[i] != line[i-1])
				temp++;
			count[temp]++;
		}

		int c = 0, top = temp;
		for(int i=1; i<=top; ++i)
		{
			if(c + count[i] <= n/2)
				c+=count[i];
			else
			{
				top = i-1;
				break;
			}
		}

		if(top<3)
		{
			printf("0 0 0\n");
			continue;
		}

		int g = 0, s = 0, b = 0;
		g = count[1];

		b = c - g;
		if(g > c/3)
		{
			printf("0 0 0\n");
			continue;
		}

		for(int i=2; i<=top; ++i)
		{
			s += count[i];
			b -= count[i];
			if(s > g)
				break;
		}

		if(b <= g)
		{
			printf("0 0 0\n");
			continue;
		}

		printf("%d %d %d\n",g,s,b);
	}
	return 0;
}
