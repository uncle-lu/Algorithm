#include<cstdio>//uncle-lu
#include<algorithm>

int n,m;
int book[1010];
int temp[10];

int main()
{
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;++i)
		scanf("%d",&book[i]);

	temp[1]=10;
	for(int i=2;i<=8;++i)
		temp[i] = temp[i-1] * 10;

	int k,num;
	for(int i=1;i<=m;++i)
	{
		scanf("%d%d",&k,&num);
		int mi=0x7f7f7f7f;
		for(int j=1;j<=n;++j)
		{
			if(book[j] % temp[k] == num)
				mi = std::min(mi, book[j]);
		}
		if(mi==0x7f7f7f7f)
			printf("-1\n");
		else
			printf("%d\n",mi);
	}

	return 0;
}
