#include<cstdio>//uncle-lu
#include<algorithm>

int line[500010];
int temp[500010];
int n;
long long int ans;

void merge_sort(int left,int right)
{
	if(left==right)return ;
	int mid = (left + right) / 2;

	merge_sort(left,mid);
	merge_sort(mid+1,right);

	for(int i=left;i<=right;i++)
		temp[i] = line[i];

	int i1 = left , i2 = mid+1;
	for(int curr=left;curr <= right;curr++)
		if(i1 == mid+1)
			line[curr] = temp[i2++];
		else if(i2 > right)
			line[curr] = temp[i1++];
		else if(temp[i1] <= temp[i2])
			line[curr] = temp[i1++];
		else
			ans+=(mid-i1+1) , line[curr]=temp[i2++];

	return ;
}

int main()
{
	scanf("%d",&n);
	for(int i=1;i<=n;++i)
		scanf("%d",&line[i]);

	merge_sort(1,n);

	printf("%lld",ans);

	return 0;
}
