#include<cstdio>
#include<algorithm>
using namespace std;

int n;
long long int ans;
int line[100010];
int sum[100010];
int temp[100010];

int merge_sort(int x,int y)
{
	if(x==y)
	{
		return 0;
	}
	int mid=(x+y)>>1;
	merge_sort(x,mid);
	merge_sort(mid+1,y);
	int head_1=x,head_2=mid+1,cnt=0;
	while(head_1<=mid&&head_2<=y)
	{
		if(sum[head_2]<sum[head_1])
		{
			temp[++cnt]=sum[head_2];
			head_2++;
			ans+=(mid-head_1+1);
		}
		else {
			temp[++cnt]=sum[head_1];
			head_1++;
		}
	}
	if(head_1<=mid)
	{
		for(int i=head_1;i<=mid;++i)
		{
			temp[++cnt]=sum[i];
		}
	}
	if(head_2<=y)
	{
		for(int i=head_2;i<=y;++i)
		{
			temp[++cnt]=sum[i];
		}
	}
	cnt=0;
	for(int i=x;i<=y;++i)
	{
		sum[i]=temp[++cnt];
	}
	return 0;
}

int main()
{
	scanf("%d",&n);
	for(int i=1;i<=n;++i)
	{
		scanf("%d",&line[i]);
	}
	for(int i=n;i>=1;--i) { sum[i]=sum[i+1]+line[i]; }
	merge_sort(1,n+1);
	printf("%lld",ans);
	return 0;
}
