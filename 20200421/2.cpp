#include<cstdio>
#include<cmath>
#include<algorithm>

struct node{
	int k,score;
};
node line[5010];
int n,m;

bool check(int x)
{
	if(line[x].score>line[x-1].score)return true;
	if(line[x].score<line[x-1].score)return false;

	if(line[x].k<line[x-1].k)return true;
	if(line[x].k>line[x-1].k)return false;

	return false;
}

int main()
{
	scanf("%d %d",&n,&m);
	m = m*1.5;
	for(int i=1;i<=n;++i)
		scanf("%d %d",&line[i].k,&line[i].score);

	for(int i=1;i<=n;++i)
		for(int j=n;j>i;--j)
			if(check(j))std::swap(line[j],line[j-1]);

	while(line[m].score==line[m+1].score)m++;

	printf("%d %d\n",line[m].score,m);
	for(int i=1;i<=m;++i)
		printf("%d %d\n",line[i].k,line[i].score);

	return 0;
}
