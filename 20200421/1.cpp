#include<cstdio>
#include<algorithm>

int line[20];

bool check(int x)
{
	if(line[x]%2==1 && line[x-1]%2!=1)return true;

	if(line[x]%2==1 && line[x-1]%2==1 && line[x] > line[x-1])return true;

	if(line[x]%2!=1 && line[x-1]%2!=1 && line[x] < line[x-1])return true;

	return false;
}

void bubble_sort()
{
	for(int i=1;i<=10;++i)
		for(int j=10;j>i;--j)
			if(check(j))std::swap(line[j],line[j-1]);
	return ;
}

int main()
{
	for(int i=1;i<=10;++i)
		scanf("%d",&line[i]);

	bubble_sort();

	for(int i=1;i<=10;++i)
		printf("%d ",line[i]);

	return 0;
}
