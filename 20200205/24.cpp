#include <cstdio>//uncle-lu

int main()
{
	int n, cnt, ans, a, b;

	scanf("%d", &n);

	ans = 0;
	cnt = 0;

	for(int i=1; i<=n; ++i)
	{
		scanf("%d%d",&a,&b);

		if( 90<=a && a<=140 && 60<=b && b<=90 )
		{
			cnt++;
		}
		else
		{
			if(cnt > ans) ans = cnt;
			cnt = 0;
		}
	}
	if(cnt > ans)ans = cnt;

	printf("%d", ans);

	return 0;
}
