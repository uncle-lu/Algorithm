#include <cstdio>//uncle-lu

int main()
{
	int n, temp, cnt;

	scanf("%d",&n);

	cnt = 0;

	for(int i=1; i<=n; ++i)
	{
		scanf("%d",&temp);

		int a = temp/1000;
		int b = temp/100%10;
		int c = temp/10%10;
		int d = temp%10;

		if( d-a-b-c > 0 )cnt++;
	}

	printf("%d",cnt);

	return 0;
}
