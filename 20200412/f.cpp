#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
int line[200010];
long long int sum[200010];

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
		read(line[i]);

	sum[1] = line[1];
	for (int i = 2; i <= n; i++) 
		sum[i] += sum[i-2] + line[i];

	if(n%2!=1)
	{
		printf("%lld",std::max(sum[n],sum[n-1]));
	}
	else
	{
		long long int ans = -0x7f7f7f7f7f;
		
		for(int i=0;i<=n-3;++i)
		{
			long long int temp = sum[i] + sum[ i%2 ? n-1 : n ] - sum[i+1];
			ans = std::max(ans, temp);
			temp = sum[i] + sum[ i%2 ? n : n-1 ] - sum[i+2];
			ans = std::max(ans, temp);
		}

		ans = std::max(ans, sum[n-1]);

		printf("%lld",ans);
	}

	return 0;
}
