#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>

/*
 * line : 原数列
 * l, r, d, : 题目要求的 对于每个区间的左端点 右端点 值
 * change : 差分数组
 */

int line[1000010], l[1000010], r[1000010], d[1000010], change[1000010];
int n, m;

bool check(int x)
{
	memset(change,0,sizeof(change));

	for (int i = 1; i <= x; i++) 
	{
		change[l[i]] += d[i];
		change[r[i]+1] -= d[i];
	}

	for (int i = 1; i <= n; i++) 
		change[i] = change[i] + change[i-1];

	for (int i = 1; i <= n; i++) 
		if(line[i] < change[i])return false; // 如果借出去的教室要比原有的多 则出现了问题

	return true;
}

int main()
{
	scanf("%d %d", &n, &m);

	for (int i = 1; i <= n; i++) 
		scanf("%d", &line[i]);

	for (int i = 1; i <= m; i++) 
		scanf("%d %d %d", &d[i], &l[i], &r[i]);

	if(check(m))
	{
		printf("0");
		return 0;
	}

	/*
	 * 二分第mid天，找到第一次出现问题的天数
	 */

	int left = 1, right = m, mid;
	while(left<=right)
	{
		mid = (left + right) >> 1;
		if(check(mid))
			left = mid+1;
		else
			right = mid-1;
	}
	printf("-1\n");
	printf("%d", left);

	return 0;
}
