#include<stdio.h>
#include<math.h>

double func(int a1, int b1, int a2, int b2)
{
	double ans = sqrt((a2-a1)*(a2-a1) + (b2-b1)*(b2-b1));

	return ans;
}

int main()
{
	int x1, x2, y1, y2;

	scanf("%d %d %d %d", &x1, &y1, &x2, &y2);

	printf("%.2lf", func(x1, y1, x2, y2));

	return 0;
}
