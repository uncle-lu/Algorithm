#include<cstdio>//uncle-lu
#include<algorithm>

int n,m;
double line[100010];
double sum[100010];
double b[100010];

int main()
{
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= n; i++) 
	{
		scanf("%lf", &line[i]);
	}

	/*
	 * eps : 实数二分的精度
	 * 
	 */

	double eps = 1e-5;
	double l = -1e6, r = 1e6;
	while(r-l > eps)
	{
		double mid = (l+r) / 2;
		for (int i = 1; i <= n; i++) 
			b[i] = line[i] - mid;

		for (int i = 1; i <= n; i++) 
			sum[i] = sum[i-1] + b[i];

		double ans = -1e10;
		double min_val = 1e10;
		for (int i = m; i <= n; i++) 
		{
			min_val = std::min(min_val, sum[i-m]);
			ans = std::max(ans, sum[i] - min_val);
		}

		if(ans>=0)
			l = mid;
		else
			r = mid;
	}

	printf("%d",(int)(r*1000));

	return 0;
}
