#include<cstdio>//uncle-lu
#include<algorithm>

int n, m;
int p[100010];
int change[100010];

int main()
{
	scanf("%d %d", &n, &m);

	for(int i=1; i<=m; ++i)
		scanf("%d", &p[i]);

	for(int i=1; i<m; ++i)
	{
		int from = std::min(p[i], p[i+1]), to = std::max(p[i], p[i+1]);
		change[from] ++;
		change[to] --;
	}

	for(int i=1; i<n; ++i)
		change[i] = change[i] + change[i-1];

	int a, b, c;
	long long int ans = 0;
	for(int i=1; i<n; ++i)
	{
		scanf("%d %d %d", &a, &b, &c);
		ans+=std::min((long long int)a*change[i],(long long int)b*change[i]+c);
	}

	printf("%lld", ans);

	return 0;
}
