#include<cstdio>//uncle-lu
#include<algorithm>

struct node{
	int l, r;
	friend bool operator<(const node a, const node b)
	{
		if(a.l == b.l)
			return a.r < b.r;
		return a.l < b.l;
	}
};
int change[10010];
int n, h, sit, r;
node list[10010], temp[10010];

int main()
{
	scanf("%d %d %d %d", &n, &sit, &h, &r);
	for(int i=1; i<=r; ++i)
	{
		scanf("%d %d", &temp[i].l, &temp[i].r);
		if(temp[i].r < temp[i].l)
			std::swap(temp[i].r, temp[i].l);
	}

	int cnt = 0 ;
	std::sort(temp+1, temp+1+r);
	for(int i=1; i<=r; ++i)
		if(temp[i].l != temp[i-1].l || temp[i].r != temp[i-1].r)
			list[++cnt] = temp[i];

	for(int i=1; i<=cnt; ++i)
	{
		change[list[i].l+1] ++;
		change[list[i].r] --;
	}

	for(int i=1; i<=n; ++i)
		change[i] = change[i] + change[i-1];

	for(int i=1; i<=n; ++i)
		printf("%d\n", h - change[i]);

	return 0;
}
