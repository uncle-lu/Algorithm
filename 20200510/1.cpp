#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int N = 5*1e5+10;
struct node{
	int num,sit;
	friend bool operator<(const node a, const node b)
	{
		if(a.num == b.num)
			return a.sit < b.sit;
		return a.num < b.num;
	}
};
int list[N], Tree[N], n;
node line[N];

int lowbit(int x)
{
	return x & (-x);
}

void update(int x, int k)
{
	while(x<=n)
	{
		Tree[x] += k;
		x += lowbit(x);
	}
	return ;
}

int query(int x)
{
	int sum = 0;
	while(x>0)
	{
		sum += Tree[x];
		x -= lowbit(x);
	}
	return sum;
}

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
	{
		read(line[i].num);
		line[i].sit = i;
	}

	std::sort(line+1, line+1+n);
	for (int i = 1; i <= n; i++) 
	{
		list[line[i].sit] = i;
	}

	long long int ans = 0;
	for(int i=1; i<=n; ++i)
	{
		update(list[i], 1);
		ans += i - query(list[i]);
	}

	printf("%lld", ans);
	return 0;
}
