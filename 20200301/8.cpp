#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
long long int line[50010];
long long int a[500010], b[500010], c[500010];
long long int ans[500010];

int main()
{
	//freopen("in","r",stdin);
	int cnt = 1;
	read(n);
	for(int i = 1; i <= n; i++)
		read(line[i]);
	while(~scanf("%lld %lld %lld", &a[cnt], &b[cnt], &c[cnt]))cnt++;

	cnt--;

	long long int lastans = -a[cnt];
	for(int i=cnt-1;i>=1;--i)
	{
		ans[i] = lastans;
		long long int x = lastans;
		lastans = -
			(a[i] * (x+1) * line[x] * line[x] + (b[i]+1)*x*line[x] + c[i] + x)
		/ //--------------------------------------------------------------------------
								( (x+1)*line[x]*line[x] + x*line[x] + 1 );
	}

	for(int i=1;i<cnt;++i)
		printf("%lld\n",ans[i]);

	//fclose(stdin);
	return 0;
}
