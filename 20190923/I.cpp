#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n,q,top;
int line[10010];

int main()
{
	//freopen("in","r",stdin);
	int temp=0,temp_1;
	read(n);read(q);
	for(int i=1;i<=n;++i)
	{
		read(temp);
		temp_1 = top;
		while(top < temp + temp_1)
			line[top++] = i;
	}

	//debug
	//for(int i=0;i<=top;++i)
	//	printf("%d ",line[i]);
	//printf("\n");

	for(int i=1;i<=q;++i)
	{
		read(temp);
		printf("%d\n",line[temp]);
	}

	//fclose(stdin);

	return 0;
}
