#include<cstdio>//uncle-lu
#include<cstring>
#include<queue>
#include<algorithm>

void solution(void);

struct node{
	int x,y;
	int models;
	int cnt;
	node(int xx,int yy,int ccnt,int mo){ x = xx; y = yy; models = mo; cnt = ccnt; }
};

std::queue<node>qu;
int n,m,start_x,start_y,end_x,end_y;
int turn_x[5]={0,1,0,-1,0};
int turn_y[5]={0,0,1,0,-1};

char map[110][110];
int cnt_map[110][110];

void solution()
{
	cnt_map[start_x][start_y] = 0;
	node now(start_x,start_y,0,0);
	qu.push(now);

	while(!qu.empty())
	{
		now = qu.front();
		qu.pop();
		
		for(int i=1;i<=4;++i)
		{
			int xx = now.x + turn_x[i], yy = now.y + turn_y[i];
			if(map[xx][yy]!='.'&&map[xx][yy]!='C')continue;
			
			if(!now.models)
			{
				node nows(xx,yy,now.cnt,i);
				if(nows.cnt <= cnt_map[xx][yy])
				{
					cnt_map[xx][yy]=nows.cnt;
					qu.push(nows);
				}
				continue;
			}
			if(now.models == i)
			{
				node nows(xx,yy,now.cnt,i);
				if(nows.cnt <= cnt_map[xx][yy])
				{
					cnt_map[xx][yy]=nows.cnt;
					qu.push(nows);
				}
			}
			else if(now.models%2 == i%2)
			{
				continue;
			}
			else
			{
				node nows(xx,yy,now.cnt+1,i);
				if(nows.cnt <= cnt_map[xx][yy])
				{
					cnt_map[xx][yy]=nows.cnt;
					qu.push(nows);
				}
			}

		}
	}

	printf("%d",cnt_map[end_x][end_y]);

	return ;
}

int main()
{
	//freopen("in","r",stdin);

	memset(cnt_map,0x7f7f7f7f,sizeof(cnt_map));
	bool flag=false;
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;++i)
	{
		scanf("\n");
		for(int j=1;j<=m;++j)
		{
			scanf("%c",&map[i][j]);
			if(map[i][j]=='C')
			{
				if(!flag) { flag = true; start_x = i; start_y = j; }
				else { end_x = i; end_y = j; }
			}
		}
	}

	solution();

	//fclose(stdin);
	return 0;
}
