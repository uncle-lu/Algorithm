#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void add_edge(int, int);
void DFS(int, int);

struct node{
	int v,next;
};
node edge[200010];
int head[30010];
int cnt,n,m,t,ans;
bool visit[30010];

void add_edge(int x,int y)
{
	cnt++;
	edge[cnt].next = head[x];
	edge[cnt].v = y;
	head[x] = cnt;
	return ;
}

void DFS(int x, int fa)
{
	ans++;
	visit[x] = true;

	for(int i=head[x];~i;i=edge[i].next)
	{
		if(edge[i].v == fa)continue;
		if(visit[edge[i].v])continue;
		DFS(edge[i].v,x);
	}

	return ;
}

int main()
{
	memset(head,-1,sizeof(head));

	int a,b,temp;
	read(n);read(m);read(t);
	for(int i=1;i<=m;++i)
	{
		read(a);read(b);
		if(a==b)continue;
		add_edge(a,b);add_edge(b,a);
	}

	for(int i=1;i<=t;++i)
	{
		read(temp);
		for(int j=head[temp];~j;j=edge[j].next)
		{
			visit[edge[j].v]=true;
		}
	}

	DFS(1,0);

	printf("%d",n-ans);
	
	return 0;
}
