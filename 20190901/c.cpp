#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int gcd(long long int,long long int);

int T;
long long int n,m;
long long int x_1,x_2,y_1,y_2;

long long int gcd(long long int a,long long int b)
{
	long long int r;
	while(b>0)
	{
		r = a % b ;
		a = b;
		b = r;
	}
	return a;
}


int main()
{
	read(n);read(m);read(T);
	long long int k = gcd(n,m);
	n = n/k;
	m = m/k;
	for(int t=1;t<=T;++t)
	{
		read(x_1);read(y_1);read(x_2);read(y_2);
		if(x_1==x_2)
		{
			if(x_1==1)
			{
				if(y_1%n==0)y_1 = y_1/n - 1;
				else y_1 = y_1/n;
				if(y_2%n==0)y_2 = y_2/n - 1;
				else y_2 = y_2/n;
				if(y_1==y_2)printf("YES\n");
				else printf("NO\n");
			}
			else
			{
				if(y_1%m==0)y_1 = y_1/m - 1;
				else y_1 = y_1/m;
				if(y_2%m==0)y_2 = y_2/m - 1;
				else y_2 = y_2/m;
				if(y_1==y_2)printf("YES\n");
				else printf("NO\n");
			}
		}
		else
		{
			if(x_2==1){std::swap(x_1,x_2);std::swap(y_1,y_2);}

			if(y_1%n==0)y_1 = y_1/n - 1;
			else y_1 = y_1/n;
			if(y_2%m==0)y_2 = y_2/m - 1;
			else y_2 = y_2/m;

			if(y_1==y_2)printf("YES\n");
			else printf("NO\n");
		}
	}
	return 0;
}
