#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;

template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9'){f|=(ch=='-');ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<1)+(x<<3)+(ch^48);ch=getchar();}
	x = f ? -x : x;
	return ;
}

int n;
int list[100010];

int main()
{
	//freopen("in","r",stdin);
	read(n);
	for(int i=1;i<=n;++i)
	{
		read(list[i]);
		if(list[i]>=0)
		{
			list[i] = -list[i]-1;
		}
	}
	if(n%2)
	{
		int mx=0,sit=-1;
		for(int i=1;i<=n;++i)
		{
			if(abs(list[i])>mx)
			{
				mx = abs(list[i]);
				sit = i;
			}
		}
		list[sit] = -list[sit] - 1;
	}
	for(int i=1;i<=n;++i)
	{
		printf("%d ",list[i]);
	}
	//fclose(stdin);
	return 0;
}
