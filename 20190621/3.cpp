#include<cstdio>//uncle-lu 20190622
#include<algorithm>

int n,q,max_sit,mx,h;
int Original_list[100010],sta[100010];
struct ans{
	int l,r;
}answer[100010];

int main()
{
	//freopen("in","r",stdin);
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&Original_list[i]);
		if(Original_list[i]>mx)
		{
			max_sit=i;
			mx=Original_list[i];
		}
	}
	h=1;
	for(int i=max_sit+1;i<=n;i++)
	{
		sta[h]=Original_list[i];
		h++;
	}
	for(int i=1;i<max_sit;i++)
	{
		if(Original_list[i]>Original_list[i+1])
		{
			answer[i].l=Original_list[i];
			answer[i].r=Original_list[i+1];
			std::swap(Original_list[i],Original_list[i+1]);
			sta[h]=Original_list[i];
			h++;
		}
		else
		{
			answer[i].l=Original_list[i];
			answer[i].r=Original_list[i+1];
			sta[h]=Original_list[i];
			h++;
		}
	}
	h--;
	long long int m;
	for(int i=1;i<=q;i++)
	{
		scanf("%I64d",&m);
		if(m<=max_sit-1){
			printf("%d %d\n",answer[m].l,answer[m].r);
		}
		else
		{
			m=m-max_sit+1;
			m%=(n-1);
			if(m==0){
				printf("%d %d\n",mx,sta[n-1]);
			}
			else{
				printf("%d %d\n",mx,sta[m]);
			}
		}
	}
	//fclose(stdin);
	return 0;
}
