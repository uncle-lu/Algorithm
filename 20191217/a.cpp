#include <cstdio>//uncle-lu
#include <cstring>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int tong[10];
char line[110];

int main()
{
	int n;
	read(n);
	for(int i=1; i<=n; ++i)
	{
		memset(tong,0,sizeof(tong));

		gets(line);

		int len = strlen(line);
		for(int j=0; j<len; ++j)
			tong[line[j] - '0'] ++;

		if(!tong[0])
		{
			printf("cyan\n");
			continue;
		}
		tong[0]--;

		int sum = 0;
		bool flag = false;
		for(int j=0; j<10;++j)
		{
			if(j%2 == 0 && tong[j])
			{
				flag = true;
			}
			sum += (j*tong[j]);
		}

		if( sum%3 == 0 && flag)
			printf("red\n");
		else
			printf("cyan\n");
	}

	return 0;
}
