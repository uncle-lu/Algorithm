#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void Euler(void);

int r, c;
int matrix[510][510];
int p[100000];
bool f[1000000000];
int cnt = 0;

void Euler()
{
	for(int i=2; cnt<=510 ; ++i)
	{
		if(!f[i])
			p[++cnt] = i;
		for(int j=1 ; j<=cnt && i*p[j] <= 1e9; ++j)
		{
			f[i*p[j]] = true;
			if(i%p[j] == 0)break;
		}
	}

	return ;
}

int main()
{
	freopen("list","w",stdout);
	Euler();
	printf("int list[%d] = {\n",cnt+10);
	printf("0");
	for(int i=1;i<=cnt;++i)
	{
		printf(",%d",p[i]);
	}
	printf("};");
	fclose(stdout);
	return 0;
}
