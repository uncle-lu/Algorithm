#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int n;

int main()
{
	int T;
	read(T);
	for(int t = 1; t <= T; ++t)
	{
		read(n);
		if( 1 <= n && n <= 6 )
		{
			printf("NO\n");
			continue;
		}
		int temp = n%14;
		if( 1<=temp && temp<=6 )
			printf("YES\n");
		else
			printf("NO\n");
	}
	return 0;
}
