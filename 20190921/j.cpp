#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int x,y;
};
node bead[210];

int n;
int line[1010];
int F[210][210];

int main()
{
	read(n);
	for(int i=1;i<=n;++i)
		read(line[i]);
	
	for(int i=1;i<=n;++i)
	{
		bead[i].x = line[i];
		bead[i].y = line[i+1];
	}
	bead[n].y = line[1];

	for(int i=1;i<=n;++i)
		bead[i+n] = bead[i];

	for(int len=2; len<=n; ++len)
	{
		for(int i=1; i<= 2*n-len+1 ; ++i)
		{
			int j = i + len - 1;

			for(int temp = i ; temp < j ; temp++)
			{
				F[i][j] = std::max(F[i][j],F[i][temp] + F[temp+1][j] + bead[i].x*bead[temp].y*bead[j].y);
			}
		}
	}

	int ans = 0 ;
	for(int i=1;i<=n;++i)
		ans = std::max(F[i][i+n-1],ans);
	printf("%d",ans);

	return 0;
}
