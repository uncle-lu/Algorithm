#include<stdio.h>
#include<string.h>

int max(int a, int b) { return a>b ? a : b; }

int max_len(char *s[], int n)
{
	int mx = 0, i;
	for(i=1; i<=n; ++i)
	{
		int len = strlen(s[i]);
		mx = max(mx, len);
	}

	return mx;
}

int main()
{
	int n, i;
	char str[11][200];
	char *s[11];
	scanf("%d\n", &n);
	for(i=1; i<=n; ++i)
	{
		scanf("%s", str[i]);
		s[i] = str[i];
	}

	printf("length=%d", max_len(s, n));

	return 0;
}
