#include<stdio.h>

int x_turn[4]={1,0,-1,0};
int y_turn[4]={0,1,0,-1};
int map[100][100];

int main()
{
	int n, i, j;
	scanf("%d",&n);

	int turn = 0, x = 1, y = 1;
	for(i=1; i<=n*n; i++)
	{
		map[x][y] = i;
		int xx = x + x_turn[turn], yy = y + y_turn[turn];
		if(map[xx][yy] || xx < 1 || xx > n || yy < 1 || yy > n)
		{
			turn++;
			turn%=4;
		}
		x = x + x_turn[turn], y = y + y_turn[turn];
	}

	for(i=1; i<=n; ++i)
	{
		for(j=1; j<=n; ++j)
		{
			if(map[i][j] < 10)
				printf("  %d", map[i][j]);
			else
				printf(" %d", map[i][j]);
		}
		printf("\n");
	}

	return 0;
}
