#include<stdio.h>
#include<string.h>

char *str_bin(char* str1, char* str2)
{
	char str3[210];
	int len_1 = strlen(str1), len_2 = strlen(str2);
	int len_3 = len_1 + len_2, curr;

	int i_1 = 0, i_2 = 0;
	for(curr=0; curr<len_3; curr++)
		if(i_1 == len_1)
			str3[curr] = str2[i_2++];
		else if(i_2 == len_2)
			str3[curr] = str1[i_1++];
		else if(str1[i_1] < str2[i_2])
			str3[curr] = str1[i_1++];
		else
			str3[curr] = str2[i_2++];
	
	str3[curr] = '\0';

	strcpy(str1, str3);

	return str1;
}

int main()
{
	char str1[210], str2[210];
	scanf("%s\n%s", str1, str2);

	str_bin(str1, str2);

	printf("%s", str1);

	return 0;
}
