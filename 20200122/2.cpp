#include<cstdio>//uncle-lu
#include<cstring>

char line[50];

int main()
{
	int t;
	scanf("%d\n",&t);

	for(int i=1; i<=t; ++i)
	{
		gets(line);
		int len = strlen(line);


		if(line[0] == 'a' || line[0] == 'e' || line[0] == 'i' || line[0] == 'o' || line[0] == 'u' )
		{
			line[len] = 'c';
			line[len+1] = 'o';
			line[len+2] = 'w';
			line[len+3] = '\0';

			puts(line);
		}
		else
		{
			line[len] = line[0];
			line[len+1] = 'o';
			line[len+2] = 'w';
			line[len+3] = '\0';

			puts(line+1);
		}
	}

	return 0;
}
