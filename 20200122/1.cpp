#include<cstdio>//uncle-lu
#include<cstring>

char line[1010];

int main()
{
	gets(line);

	int len = strlen(line);
	for(int i=0; i<len; ++i)
		if(line[i] >= 'A' && line[i] <= 'Z')
			line[i] = line[i] - 'A' + 'a';

	puts(line);

	return 0;
}
