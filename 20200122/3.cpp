#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>

char line_1[100];
char line_2[100];

int main()
{
	gets(line_1); gets(line_2);
	int len_1 = strlen(line_1), len_2 = strlen(line_2);

	int ans = 0;

	for(int i = len_1-1; i>0; i--)
	{
		if(line_1[i] == line_2[0])
		{
			bool flag = true;

			for(int j=1; i+j < len_1; ++j)
				if(line_1[i+j] != line_2[j])
				{
					flag = false;
					break;
				}

			if(flag)
			{
				ans = std::max(ans, len_1-i);
			}
		}
	}

	for(int i = len_2-1; i>0; i--)
	{
		if(line_2[i] == line_1[0])
		{
			bool flag = true;

			for(int j=1; i+j < len_2; ++j)
				if(line_2[i+j] != line_1[j])
				{
					flag = false;
					break;
				}

			if(flag)
			{
				ans = std::max(ans, len_2-i);
			}
		}
	}

	printf("%d",ans);
	return 0;
}
