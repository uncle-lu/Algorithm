#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>
void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void solution(void);
bool check(int, int, int);

int n,m,s;
int line[500010];
int temp[500010],copy[500010];

bool check(int l, int r, int p)
{
	for(int i=r+1;i<=r+p;++i)
		temp[i] = line[i];

	std::sort(temp+r+1,temp+r+p+1);
	int i1 = l,i2 = r+1;
	for(int current = l ; current <= r+p ; ++current)
	{
		if(i1>r)
			copy[current] = temp[i2++];
		else if(i2>r+p)
			copy[current] = temp[i1++];
		else if(temp[i1] < temp[i2])
			copy[current] = temp[i1++];
		else
			copy[current] = temp[i2++];
	}

	int tot=0,len = r+p-l+1;
	/*
	printf("l = %d, r = %d, p = %d, len = %d\n",l,r,p,len);
	printf("temp=");
	for(int i = l ; i <= r+p; ++i)
		printf("%d ",temp[i]);
	printf("\n");
	*/
	for(int current = 0 ; current < len/2 && current < m ; ++current)
		tot +=( (copy[l + current] - copy[r+p-current]) * (copy[l + current] - copy[r+p-current]) );

	for(int current = l ; current <= r+p ; ++current)
		temp[current] = copy[current];

	if(tot > s)
		return false;
	else
		return true;
}

void solution()
{
	memset(temp,0,sizeof(temp));
	memset(copy,0,sizeof(copy));
	//printf("***\n");
	int k=1,tot=0;
	while(k<=n)
	{
		tot++;
		int p=1,l=k,r=k;
		temp[l] = line[l];
		while(p)
		{
			if(r + p <= n && check(l,r,p) )
			{
				r += p;
				p *= 2;
			}
			else
				p /= 2;
		}
		k = r + 1;
		//printf("k=%d\n",k);
	}

	printf("%d\n",tot);

	return ;
}

int main()
{
	freopen("in","r",stdin);

	int T;
	read(T);
	for(int t=1;t<=T;++t)
	{
		read(n);read(m);read(s);

		for(int i=1;i<=n;++i)
			read(line[i]);

		solution();
	}

	fclose(stdin);
	return 0;
}
