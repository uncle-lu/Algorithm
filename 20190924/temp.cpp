#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int S[110];
int n,t;

int main()
{
	freopen("in","r",stdin);

	read(n);
	for(int i=1;i<=n;++i)
	{
		read(S[i]);
		S[i] += S[i-1];
	}
	read(t);

	int sum=0,p=1,k=0;
	while(p)
	{
		if( sum+S[k+p]-S[k] <= t && k+p <= n)
		{
			sum += (S[k+p] - S[k]);
			k += p;
			p *= 2;
		}
		else
		{
			p /= 2;
		}
	}

	printf("%d",k);

	fclose(stdin);

	return 0;
}
