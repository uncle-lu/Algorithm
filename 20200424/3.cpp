#include <bits/stdc++.h>

using namespace std;

const int inf = 0x3f3f3f3f;

template<typename T> inline void cmin(T &x, const T &y) {
	x = min(x, y);
}

int main() {
	int n, x, y;
	scanf("%d %d %d", &n, &x, &y);
	vector<int> a(n), c(3);
	for (int i = 0; i < n; ++i) {
		int z;
		scanf("%d", &z);
		if (z == x) {
			a[i] = 0;
		} else if (z == y) {
			a[i] = 1;
		} else {
			a[i] = 2;
		}
		++c[a[i]];
	}
	if (!c[0] || !c[1]) {
		puts("0");
		return 0;
	}
	if (!c[2]) {
		puts("-1");
		return 0;
	}
	int answer = n;
	for (int remove = 0; remove < 2; ++remove) {
		bool flag = a[0] == 2 || a[n - 1] == 2;
		int last = 2;
		for (int i = 0; i < n; ++i) {
			if (a[i] != remove) {
				if (a[i] == 2 && last == 2) {
					flag = true;
				}
				last = a[i];
			}
		}
		if (last == 2) {
			flag = true;
		}
		if (flag) {
			answer = min(answer, c[remove]);
		} else {
			answer = min(answer, c[remove] + 1);
		}
	}
	vector<vector<int>> f(n + 1, vector<int> (12, inf));
	f[c[0] + c[1]][2] = 0;
	for (int i = 0; i < n; ++i) {
		vector<vector<int>> g(n + 1, vector<int> (12, inf));
		for (int j = 0; j <= n; ++j) {
			for (int state = 0; state < 4; ++state) {
				for (int last = 0; last < 3; ++last) {
					if (f[j][state * 3 + last] != inf) {
						if (a[i] == 2) {
							cmin(g[j + 1][state * 3 + last], f[j][state * 3 + last] + 1);
							cmin(g[j][state * 3 + 2], f[j][state * 3 + last]);
						} else {
							cmin(g[j][state * 3 + last], f[j][state * 3 + last] + 1);
							cmin(g[j - (last + a[i] == 1)][(state | 1 << a[i]) * 3 + a[i]], f[j][state * 3 + last]);
						}
					}
				}
			}
		}
		f = g;
	}
	for (int i = c[0] + c[1]; i <= n; ++i) {
		for (int j = 9; j < 12; ++j) {
			answer = min(answer, f[i][j]);
		}
	}
	printf("%d\n", answer);
	return 0;
}
