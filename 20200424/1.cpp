#include <bits/stdc++.h>
using namespace std;
 
#define ll long long
#define N 1000010
/*
 * prime 存储质数
 * check[i] 判断i是否为素数
 * vec 存储每个数的质因数
 */
int n, a[N], nx[N]; 
int prime[N], tot, check[N];
vector <vector<int>> vec;
void init() {
	memset(check, 0, sizeof check);
	tot = 0;
	for (int i = 2; i < N; ++i) {//线性筛素数
		if (!check[i]) {
			prime[++tot] = i;
		}
		for (int j = 1; j <= tot; ++j) {
			if (1ll * i * prime[j] >= N) break;
			check[i * prime[j]] = 1;
			if (i % prime[j] == 0) break;
		}
	}
	vec.clear(); vec.resize(N);
	for (int i = 1; i <= tot; ++i) {//统计每个数j的各个质因数
		for (int j = prime[i]; j < N; j += prime[i]) {
			vec[j].push_back(prime[i]);
		}
	}
}
 
int main() {
	init();
	while (scanf("%d", &n) != EOF) {
		for (int i = 1; i <= n; ++i) {
			scanf("%d", a + i);
		}
		for (int i = 1; i <= 1000000; ++i) {
			nx[i] = n + 1; 
		}
		ll res = 0, base = 0;
		/*
		 * nx[i] 表示质因数i 上次出现在什么位置
		 */
		for (int i = n; i >= 1; --i) {
			for (auto it : vec[a[i]]) { //对于每个a[i] 统计它自身的质因数所带来的贡献
				base += (nx[it] - i);
				nx[it] = i;//更新质因数的位置
			}
			res += base;
		}
		printf("%lld\n", res);
	}
	return 0;
}
