#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n,cnt_minus;
long long int temp,count;
bool flag = true;

int main()
{
	read(n);
	for(int i=1;i<=n;++i)
	{
		read(temp);
		if(temp<0)
		{
			cnt_minus++;
			count += (-1 - temp);
		}
		else if(temp>0)
		{
			count += (temp - 1);
		}
		else
			count ++ , flag=false;
	}
	if(cnt_minus % 2 && flag)count += 2;

	printf("%lld",count);

	return 0;
}
