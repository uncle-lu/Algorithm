#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n_a,n_b;
int list_a[210],list_b[210];
bool visit_sum[410];

int main()
{
	read(n_a);
	for(int i=1;i<=n_a;++i)read(list_a[i]);
	read(n_b);
	for(int i=1;i<=n_b;++i)read(list_b[i]);

	for(int i=1;i<=n_a;++i)
		visit_sum[list_a[i]] = true;

	for(int i=1;i<=n_b;++i)
		visit_sum[list_b[i]] = true;

	for(int i=1;i<=n_a;++i)
		for(int j=1;j<=n_b;++j)
		{
			if(!visit_sum[ list_a[i] + list_b[j] ])
			{
				printf("%d %d",list_a[i],list_b[j]);
				return 0;
			}
		}

	return 0;
}
