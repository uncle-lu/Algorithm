#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void init(void);
bool DFS(int);

struct node{
	long long int x,y,z;
};

node line[1010];
bool visit[1010];
long long int r, h;
int n;

void init()
{
	memset(visit,0,sizeof(visit));
	return ;
}

long long int dis(int x,int y)
{
	return (line[x].x-line[y].x)*(line[x].x-line[y].x) + (line[x].y-line[y].y)*(line[x].y-line[y].y) + (line[x].z-line[y].z)*(line[x].z-line[y].z);
}

bool DFS(int x)
{
	visit[x] = true;
	if(line[x].z+r>=h)return true;
	for(int i=1;i<=n;++i)
	{
		if(visit[i])continue;
		if(dis(x,i) <= (long long int)4*r*r)
		{
			if(DFS(i))return true;
		}
	}
	return false;
}

int main()
{
	int T;
	read(T);
	while(T--)
	{
		init();
		read(n);read(h);read(r);

		for(int i=1;i<=n;++i)
		{
			read(line[i].x);read(line[i].y);read(line[i].z);
		}
		bool flag=false;
		for(int i=1;i<=n && !flag ;++i)
		{
			if(line[i].z<=r && !visit[i])
			{
				if(DFS(i))flag = true;
			}
		}
		if(flag)
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}
