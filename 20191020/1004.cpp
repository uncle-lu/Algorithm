#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int F[11][11][11][11];
int map[11][11];
int n;

int main()
{
	int x,y,z;
	read(n);
	while(true)
	{
		read(x);read(y);read(z);
		if(!x && !y && !z)break;
		map[x][y] = z;
	}

	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			for(int a=1;a<=n;++a)
			{
				int b = i + j - a;
				if(b<=0)break;

				F[i][j][a][b] = std::max(F[i-1][j][a-1][b],std::max(F[i][j-1][a-1][b],std::max(F[i-1][j][a][b-1],F[i][j-1][a][b-1])));
				if(i==a && j==b)
					F[i][j][a][b] += map[i][j];
				else
					F[i][j][a][b] += (map[i][j] + map[a][b]);
			}
		}
	}

	printf("%d",F[n][n][n][n]);
	return 0;
}
