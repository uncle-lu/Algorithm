#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int F[10010][1010];
int Up[10010], Down[10010];
int High[10010], Low[10010];
int n, m, k;

int main()
{
	read(n);read(m);read(k);
	for(int i=1;i<=n;++i)
	{
		read(Up[i]);read(Down[i]);
	}

	for(int i=1;i<=n;++i) { High[i] = m+1; Low[i] = 0; }

	for(int i=1;i<=k;++i) { read(p); read(Low[p]); read(High[p]); High }

	memset(F,0x7f7f7f7f,sizeof(F));
	for(int i=1;i<=m;++i)
		F[0][i] = 0;

	for(int i=1;i<=n;++i)
	{
		for(int j=m;j>=m-Up[i];--j)
			F[i][m] = std::min(F[i-1][j]+1,F[i][m]);

		for(int j=1;j<=m-Up[i];++j)
			F[i][m] = std::min(F[i-1][j]+1,F[i][m]);

		for(int j=1;j<=m;++j)
		{
			if(j > m - Up[i])
				F[i][m] = std::min(F[i][m],F[i][j]+1);
			else
				F[i][j+Up[i]] = std::min(F[i][j+Up[i]],F[i][j]+1);
		}

		for(int j=m-Down[i];j>=1;--j)
			F[i][j] = std::min(F[i][j],F[i-1][j+Down[i]]);

		for(int j=1;j<=Low[i];++j)
			F[i][j] = 0x7f7f7f7f;
		for(int j=m;j>=High[i];++j)
			F[i][j] = 0x7f7f7f7f;
	}

	
}
