#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;bool f=false;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x = (x<<1) + (x<<3) + (ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct operation{
	int cnt,top;
	bool models[10];
	int target[10];
};

void init(void);
void solution(void);

int n;
operation mpi[10010];

void solution()
{
	// read
	for(int i=1;i<=n;++i)
	{
		gets(line);
		int len = strlen(line);
		int temp = 0, mpi[i].cnt = 1; 
		for(int it=0;it<len;++it)
		{
			if(line[it]<='9'&&line[it]>='0')temp = (temp<<1) + (temp<<3) + (line[it]^48);
			else if(line[it]==' ')
			{
				mpi[i].target[ mpi[i].cnt ]=temp;
				temp=0;
				mpi[i].cnt++;
			}
			else if(line[it]=='R')
				mpi[i].models[ mpi[i].cnt ]=false;
			else if(line[it]=='S')
				mpi[i].models[ mpi[i].cnt ]=true;
		}
	}

	
}

int main()
{
	freopen("in","r",stdin);
	int T;
	read(T);read(n);
	for(int t=1;t<=T;t++)
	{
		init();
		solution();
	}
	fclose(stdin);
	return 0;
}
