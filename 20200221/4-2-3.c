/*
 * Author: uncle-lu 21190603
 * Mail: uncle-lu@qq.com
 * Blog: uncle-lu.org
 * File name: 4-2-3.c
 * Description: C course assignments.
 */
#include <stdio.h>

int main()
{
	int Month[13]={
		0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
	};
	int year, month;

	scanf("%d%d", &year, &month);

	if( month == 2 )
	{
		if( year%100 == 0 )
		{
			if( (year/100)%4 == 0)
				Month[2]++;
		}
		else if( year%4 == 0)
		{
			Month[2]++;
		}
	}

	printf("%d",Month[month]);

	return 0;
}
