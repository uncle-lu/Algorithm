/*
 * Author: uncle-lu 21190603
 * Mail: uncle-lu@qq.com
 * Blog: uncle-lu.org
 * File name: 1-5.cpp
 * Description: C++ course assignments.
 */
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

bool cmp_1(std::string, std::string);
bool cmp_2(std::string, std::string);

int main()
{
	std::vector<std::string> line;
	std::string str;

	while(std::cin >> str)
		line.push_back(str);

	std::sort(line.begin(), line.end(), cmp_1);
	for(auto i:line)
	{
		std::cout << i << std::endl;
	}

	std::cout << "*****************" << std::endl;

	std::sort(line.begin(), line.end(), cmp_2);
	for (auto i : line) {
		std::cout << i << std::endl;
	}
}

bool cmp_1(std::string a, std::string b)
{
	return a<b;
}

bool cmp_2(std::string a, std::string b)
{
	return a.length() < b.length();
}
