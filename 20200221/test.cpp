/*
 * Author: uncle-lu 21190603
 * Mail: uncle-lu@qq.com
 * Blog: uncle-lu.org
 * File name: test.cpp
 * Description: C++ course assignments.
 * 2020.02.20
 */
#include<iostream>
#include<string>

int strToint(const std::string& str)
{
	int num = 0;
	//Throw Max
	std::string mx("2147483647");
	if( str.length() > mx.length() )
		throw std::string("out of range!");
	if( str.length() == mx.length() && mx.compare(str) < 0 )
		throw std::string("out of range!");

	bool flag = false; // Negative number 
	for (unsigned int i = 0; i < str.length(); i++)
	{
		if ( (str[i] >= '0') && (str[i] <= '9') )
		{
			num = num * 10 + str[i] - '0';
		}
		else if(i==0 && str[i]=='-')
		{
			flag = true;
		}
		else
		{
			throw str[i];
		}
	}
	return flag ? -num : num;
}

int main()
{
	std::string str;
	int i_value;
	std::cout <<"Please input a number string:\n";
	std::cin >> str;
	try
	{
		i_value = strToint(str);
		if (i_value > 10000) throw i_value;
	}
	catch (char err)
	{
		std::cout <<"Input string includes illegal character：" << err << std::endl;
		i_value = -1;
	}
	catch (int err)
	{
		std::cout <<"number is too big: "<< err << std::endl;
	}
	catch (std::string err)
	{
		std::cout << err << std::endl;
	}
	std::cout << i_value << std::endl;
	return 0;
}
