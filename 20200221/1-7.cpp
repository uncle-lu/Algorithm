/*
 * Author: uncle-lu 21190603
 * Mail: uncle-lu@qq.com
 * Blog: uncle-lu.org
 * Git: https://gitee.com/uncle-lu
 * File name: 1-7.cpp
 * Description: C++ course assignments.
 */
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

void read(std::vector<int>&);//Task 2
void del_24(std::vector<int>);//Task 3
void print_txt(std::vector<int>);//Task 4
void print(std::vector<int>, std::ostream&);

int main()
{
	std::vector<int> line;

	read(line);

	//Task 
	for (int i = 0; i < (int)line.size(); i++) 
	{
		std::cout << std::setw(5) << line[i];
		if( (i+1) % 8 == 0)
			std::cout << "\n";
	}

	//print_txt(line);
	
	//del_24(line);
	
	return 0;
}

//Task 3
void del_24(std::vector<int> line)
{
	std::vector<int> new_line;
	for (auto i : line)
	{
		bool flag = true;
		int temp = i;

		while(temp != 0)
		{
			if( temp%10 == 2 || temp%10 == 4)
			{
				flag = false;
				break;
			}
			temp /= 10;
		}

		if(flag)
			new_line.push_back(i);
	}

	print(new_line, std::cout);

	return ;
}

//Task 1
void read(std::vector<int> &line)
{
	std::ifstream in;
	in.open("data.txt");
	if(!in.is_open())
	{
		std::cout << "Can't open file: data.txt" << std::endl;
		exit(0);
	}

	int temp;
	while(in >> temp)
		line.push_back(temp);

	in.close();

	return ;
}

//Task 4
void print_txt(std::vector<int>line)
{
	std::ofstream out;
	out.open("result.txt");
	if(!out.is_open())
	{
		std::cout << "Can't open file: result.txt " << std::endl;
		exit(0);
	}

	print(line, out);

	out.close();

	return ;
}

void print(std::vector<int>line, std::ostream &out)
{
	for (int i = 0; i < (int)line.size(); i++) 
	{
		out << std::setw(5) << line[i];
		if( (i+1) % 8 == 0 )
			out << "\n";
	}

	return ;
}
