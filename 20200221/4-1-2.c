/*
 * Author: uncle-lu 21190603
 * Mail: uncle-lu@qq.com
 * Blog: uncle-lu.org
 * File name: 4-1-2.c
 * Description: C course assignments.
 */
#include <stdio.h>

void swap(int *x,int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
	return ;
}

int main()
{
	int a, b, c;
	scanf("%d%d%d", &a, &b, &c);

	if(a>b)swap(&a, &b);
	if(b>c)swap(&b, &c);
	if(a>b)swap(&a, &b);

	printf("%d %d %d", a, b, c);

	return 0;
}
