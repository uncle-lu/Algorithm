#include<cstdio>//uncle-lu
#include<iostream>
#include<string>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;bool f=false;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x = (x<<1) + (x<<3) + (ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	char val;
	int cnt;
	node *next[26];
	node(char v='\0'){ val = v; cnt = 0; }
};
struct Trie{
	node root;
	node U[1000010],*Trash[100010],*ALL = U;
	int top;

	node* ALLOC(const char k)
	{
		return new(top?Trash[top--]:ALL++)node(k);
	}

	void insert(std::string l)
	{
		node *now = &root;
		for(auto c : l)
		{
			for(int i=0;i<26;++i)
			{
				if(now->next[i] == nullptr)
				{
					now->next[i] = ALLOC(c);
					now = now->next[i];
					break;
				}
				else if(now->next[i]->val == c)
				{
					now = now->next[i];
					break;
				}
			}
		}
		now->cnt ++;
		return ;
	} 

	int find_pre(std::string l)
	{
		int ans=0;
		node *now = &root;
		for(auto c : l)
		{
			ans += now->cnt;
			for(int i=0;i<26;++i)
			{
				if(now->next[i] == nullptr)
					return ans;
				else if(now->next[i]->val == c)
				{
					now = now->next[i];
					break;
				}
			}
		}
		ans += now->cnt;
		return ans;
	}

};

Trie T;
int n,m;

int main()
{
	std::string line;
	read(n);read(m);
	for(int i=1;i<=n;++i)
	{
		std::cin >> line ; 
		T.insert(line);
	}
	
	for(int i=1;i<=m;++i)
	{
		std::cin >> line;
		std::cout << T.find_pre(line) << std::endl;
	}

	return 0;
}
