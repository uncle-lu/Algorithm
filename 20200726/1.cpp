#include<cstdio>//uncle-lu
#include<algorithm>
#include<queue>

int n, m;
long long int a[30010];
int b[30010];

std::priority_queue<long long int, std::vector<long long int>, std::greater<long long int> >qu1;
std::priority_queue<long long int, std::vector<long long int> >qu2;

int main()
{
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= n; i++) 
		scanf("%lld", &a[i]);
	for (int i = 1; i <= m; i++) 
		scanf("%d", &b[i]);

	for (int i = 1; i <= m; i++) 
	{
		for (int j = b[i-1]+1; j <= b[i]; j++) 
		{
			qu2.push(a[j]);
			qu1.push(qu2.top());
			qu2.pop();
		}
		qu2.push(qu1.top());
		qu1.pop();
		printf("%lld\n", qu2.top());
	}
	return 0;
}
