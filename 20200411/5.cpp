#include<cstdio>//uncle-lu
#include<cstring>

char line[10010];
int flag[10010];

int main()
{
	scanf("%s",line+1);

	int len = strlen(line+1);

	for(int i=1; i<=len; ++i)
	{
		if(line[i]=='(')flag[i] = 1;
		else flag[i] = -1;
		flag[i] = flag[i] + flag[i-1];
	}

	bool f = true;
	for(int i=1; i<=len; ++i)
	{
		if(flag[i]<0)f = false;
	}
	if(flag[len]!=0)f = false;

	if(f)
		printf("Yes");
	else
		printf("No");

	return 0;
}
