#include<cstdio>//uncle-lu

void han(int n,char A,char B,char C)
{
	if (n==1)
	{
		printf("Move disk %d from %c to %c\n", n, A, C);
	}
	else
	{
		han(n-1,A,C,B);
		printf("Move disk %d from %c to %c\n", n, A, C);
		han(n-1,B,A,C);
	}
}

int main()
{
	int n;
	scanf("%d",&n);
	han(n,'A','B','C');
	return 0;
}
