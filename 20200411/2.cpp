#include<cstdio>//uncle-lu

int n, top;
int line[100];

void func(int last)
{
	if(n == 0)
	{
		if(top==1)return ;
		printf("%d",line[1]);
		for(int i=2; i<=top; ++i)
			printf("+%d", line[i]);
		printf("\n");
		return ;
	}

	for(int i=last; i<=n; ++i)
	{
		line[++top] = i;
		n -= i;
		func(i);
		n += i;
		top--;
	}

	return ;
}

int main()
{
	scanf("%d",&n);
	func(1);
	return 0;
}
