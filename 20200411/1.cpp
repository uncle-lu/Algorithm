#include<cstdio>//uncle-lu

void func(int);
void print(void);

int n;
int line[10];
bool visit[10];

void print()
{
	for(int i=1; i<=n; ++i)
		printf("%d ", line[i]);
	printf("\n");
	return ;
}

void func(int x)
{
	if(x>n)
	{
		print();
		return ;
	}
	for(int i=1; i<=n; ++i)
	{
		if(visit[i])continue;
		line[x] = i;
		visit[i] = true;
		func(x+1);
		visit[i] = false;
	}
	return ;
}

int main()
{
	scanf("%d",&n);
	func(1);
	return 0;
}
