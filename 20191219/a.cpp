#include <cstdio>//uncle-lu
#include <cstring>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 110;

bool check();

char line_1[INF];
int count_1[30];
char line_2[INF];
int count_2[30];
char temp[INF];

bool check()
{
	for(int i=0;i<30;++i)
		if(count_1[i] != count_2[i])return false;

	return true;
}

int main()
{
	int T;
	read(T);
	for(int t=1; t<=T; ++t)
	{
		memset(count_1,0,sizeof(count_1));
		memset(count_2,0,sizeof(count_2));
		gets(line_1); gets(line_2);
		int len_1 = strlen(line_1), len_2 = strlen(line_2);
		if(len_2 < len_1) { printf("NO\n"); continue; }

		for(int i=0;i<len_1;++i)
			count_1[ line_1[i] - 'a' ] ++;

		for(int i=0;i<len_1;++i)
			count_2[ line_2[i] - 'a' ] ++;

		bool flag = false;
		for(int i=0; i < len_2 - len_1; ++i)
		{
			if(check())
			{
				flag = true;
				break;
			}

			count_2[ line_2[i] - 'a' ] --;
			count_2[ line_2[i+len_1] - 'a' ] ++;
		}

		if(check())
			flag = true;

		if(flag)
			printf("YES\n");
		else
			printf("NO\n");

	}
	return 0;
}
