#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 50010;

int n, m;
int line[INF];

int main()
{
	read(n);read(m);

	int first = 1, last = n;
	for(int i=1; i<=n; ++i)
	{
		if(m <= (long long int)(n-i)*(n-i-1)/2 )
		{
			line[first++] = i;
		}
		else
		{
			line[last--] = i;
			m -= last - first + 1;
		}
	}

	for(int i=1; i<=n; ++i)
		printf("%d ",line[i]);

	return 0;
}
