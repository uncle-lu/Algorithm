#include <cstdio>//uncle-lu
#include <cstring>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 1e5+10;
void init(void);

int n, m;
int line[INF], sit[INF];

void init()
{
	n = 0; m = 0;
	memset(line, 0, sizeof(line));
	memset(sit, 0, sizeof(sit));
	return ;
}

int main()
{
	int T;
	read(T);
	for(int t=1; t<=T; ++t)
	{
		init();
		read(n);read(m);
		for(int i=1; i<=n; ++i)
		{
			read(line[i]);
			sit[line[i]] = i;
		}
		
		int temp, mx = 0;
		long long int ans = 0;
		for(int i=1; i<=m; ++i)
		{
			read(temp);
			if(sit[temp] > mx)
			{
				ans += 2 * (sit[temp] - i ) + 1;
				mx = sit[temp];
			}
			else
				ans ++;
		}

		printf("%lld\n", ans);
	}
	return 0;
}
