#include <cstdio>//uncle-lu
#include <cstring>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 1e5 * 2 + 10;

int n, p, k;
int line[INF];

int main()
{
	int T;
	read(T);
	for(int t=1; t<=T; ++t)
	{
		read(n);read(p);read(k);

		for(int i=0; i<n; ++i)
			read(line[i]);

		std::sort(line,line+n);

		int ans = 0, pref = 0;
		for(int i=0; i<k; ++i)
		{
			int sum = pref;

			if(sum > p)break;

			int cnt = i;
			
			for(int j=i+k-1; j<n && sum+line[j] <= p; j+=k)
			{
				sum+=line[j];
				cnt+=k;
			}

			pref+=line[i];
			ans = std::max(ans, cnt);
		}

		printf("%d\n",ans);
	}
	return 0;
}
