#include<cstdio>//uncle-lu
#include<cstring>

char line[200010];
char s[200010];

int main()
{
	scanf("%s",line);
	int top = 0;
	int len = strlen(line);

	for(int i=0; i<len; ++i)
	{
		if(top && s[top-1] == line[i])top--;
		else s[top++] = line[i];
	}

	s[top] = '\0';

	puts(s);

	return 0;
}
