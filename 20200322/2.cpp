#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int line[100010];
int l[100010],r[100010];
int n;

int main()
{
	read(n);
	for(int i=1;i<=n;++i)
		read(line[i]);
	int fa, temp;
	for(int i=2;i<=n;++i)
	{
		read(fa);read(temp);
		( temp ? l[fa] : r[fa] ) = i; 
	}
	BFS();
}
