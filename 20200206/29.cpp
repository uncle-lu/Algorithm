#include<cstdio>

int main()
{
	int n, ans;
	scanf("%d",&n);
	ans = 0;

	bool flag = false;
	if(n<0)
	{
		flag = true;
		n = -n;
	}

	while(n!=0)
	{
		ans = ans * 10 + n % 10;
		n = n / 10;
	}

	if(flag)
		ans = -ans;
	printf("%d",ans);
	
	return 0;
}
