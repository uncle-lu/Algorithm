#include<cstdio>//uncle-lu

int main()
{
	int m, k, count;
	scanf("%d %d", &m, &k);

	count = 0;

	if(m%19 != 0)
	{
		printf("NO");
		return 0;
	}

	while(m!=0)
	{
		if(m%10 == 3)count++;
		m = m/10;
	}

	if(k == count)
		printf("YES");
	else
		printf("NO");

	return 0;
}
