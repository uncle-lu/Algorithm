#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>

char s[2010], t[2010];
int main()
{
	//freopen("in","r",stdin);
	while(~scanf("%s\n%s", s, t))
	{
		int len_s = strlen(s), len_t = strlen(t);
		int same = 0, ans = -1;
		int i, j, i1, i2;
		bool flag = false;
		for(j=0; j<len_t; ++j)
		{
			same = 0;
			i1 = 0; i2 = 0;
			for(i=0; i<len_s; ++i)
			{
				if(s[i] < t[j])
				{
					i1 = i; i2 = j;
					flag = true;
					break;
				}
				if(s[i] == t[j])
				{
					i1 = i; i2 = j;
					while(i1<len_s && i2<len_t && s[i1] == t[i2])
					{
						i1++;i2++;same++;
					}
					if(s[i1] < t[i2] || i1 >= len_s || i2 >= len_t)
					{
						flag = true;
						break;
					}
				}
			}

			if(flag)
			{
				ans = std::max(ans, (len_s - i1) + (len_t - i2) + same*2);
				flag = false;
			}
		}

		printf("%d\n", ans);
	}

	return 0;
}
