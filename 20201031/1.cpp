#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>

char s[2020],t[2020];
int F[2020][2020];

int main()
{
	freopen("geometry.in","r",stdin);//输入
	freopen("geometry.out","w",stdout);//输出

	while(~scanf("%s\n%s", s+1, t+1))
	{
		int len_s = strlen(s+1), len_t = strlen(t+1);
		memset(F, 0, sizeof(F));

		for(int i=1; i<=len_s+1; ++i)
		{
			for(int j=1; j<=len_t+1; ++j)
			{
				F[i][j] = std::max(F[i-1][j], F[i][j-1]);
				if(s[i] == t[j])
					F[i][j] = std::max(F[i][j], F[i-1][j-1] + 1);
			}
		}

		int ans = 0;
		for(int i=1; i<=len_s; ++i)
			for(int j=1; j<=len_t; ++j)
			{
				if(s[i] < t[j])
					ans = std::max(ans, F[i-1][j-1]*2 + (len_s - i + 1) + (len_t - j + 1));
				ans = std::max(ans, F[i][j]*2 + len_t - j);
			}

		for(int i=1; i<=len_s; ++i)
			ans = std::max(ans, F[i][len_t]*2);

		for(int i=1; i<=len_t; ++i)
			ans = std::max(ans, F[len_s][i]*2);

		ans = std::max(ans, len_t);

		printf("%d\n", ans);
	}

	fclose(stdin);
	fclose(stdout);
	return 0;
}
