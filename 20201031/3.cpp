#include<cstdio>
#include<map>
#include<algorithm>

int n;
struct node{
	int line[100010];
	friend bool operator<(const node a, const node b)
	{
		for(int i=1; i<=n; ++i)
		{
			if(a.line[i] < b.line[i])
				return true;
			else if(a.line[i] > b.line[i])
				return false;
		}
		return false;
	}
};
std::map<node, node>visit;

void DFS(node k)
{
	for(int i=1; i<=n; ++i)
	{
		node now = k;
		for(int j=1; j<=i/2; ++j)
			std::swap(now.line[j], now.line[i-j+1]);
		for(int j=1; j<=(n-i)/2; ++j)
			std::swap(now.line[i+j], now.line[n-j+1]);

		if(visit.count(now)==0)
		{
			visit[now] = now;
			DFS(now);
		}
	}

	return ;
}

int main()
{
	freopen("in","r",stdin);
	while(~scanf("%d", &n))
	{
		visit.clear();
		node now;
		for(int i=1; i<=n; ++i)
			scanf("%d", &now.line[i]);
		visit[now] = now;
		DFS(now);
		printf("%d\n", visit.size());
	}
	return 0;
}
