#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

typedef long long ll;
const int maxn = 1e6 + 10;

const ll mod = 1e9 + 7;

ll f[2][1005][1005];
int L[1005];
int n, m;

ll md(ll a){
	while(a>mod) a-=mod;
	return a;
}

int main()
{
    while(~scanf("%d%d", &n, &m)){
        memset(L, 0, sizeof(L));
        memset(f, 0, sizeof(f));

        while(m --){
            int a, b;
			read(a);read(b);
            L[b] = std::max(L[b], a);
        }

        f[0][0][0] = 1;
        for(int i = 0; i < n; ++i){
            for(int j = 0; j <= i; ++j){
                for(int k = 0; k <= j; ++k){
                    if(L[i] > k)    continue;
                    f[(i+1)&1][j][k]=md(f[(i + 1)&1][j][k] + 6 * f[i&1][j][k]);
                    f[(i+1)&1][i+1][j]=md(f[(i + 1)&1][i + 1][j] + 2 * f[i&1][j][k]);
                    f[(i+1)&1][i+1][i+1]=md(f[(i + 1)&1][i + 1][i + 1] + 2 * f[i&1][j][k]);
                }
            }
			for(int j=0; j<=i; ++j)
				for(int k=0;k<=j; ++k)
					f[i&1][j][k] = 0;
        }

        ll ans = 0;
        for(int i = 0; i <= n; ++i){
            for(int j = 0; j <= i; ++j){
                if(L[n] > j)    continue;
                ans = md(ans + f[n&1][i][j]);
            }
        }
        printf("%lld\n", ans);
    }
    return 0;
}
