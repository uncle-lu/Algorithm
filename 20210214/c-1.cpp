#include<cstdio>//uncle-lu 701 c
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int main()
{
	int T, x, y;
	read(T);
	for (int t = 1; t <= T; t++) 
	{
		read(x);read(y);
		long long int ans = 0;
		for (int k = 1; k <= sqrt(x); k++) 
			ans += std::max(0, std::min(y, x/k - 1) - k);
		printf("%lld\n", ans);
	}
	return 0;
}
