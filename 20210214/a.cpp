#include<cstdio>//uncle-lu 701
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int main()
{
	int a, b, T, A;
	read(T);
	for (int t = 1; t <= T; t++) 
	{
		read(a);read(b);
		int ans = 0x7f7f7f;
		bool flag = false;

		if(b == 1) { b++; flag = true; }

		for(int B=0; B<=30; ++B)
		{
			int cnt = B;
			A = a;
			while(A)
			{
				A/=(b+B);
				cnt++;
			}
			ans = std::min(ans, cnt);
		}

		if(flag)
			ans++;
		printf("%d\n", ans);
	}
	return 0;
}
