#include<cstdio>//uncle-lu 701 
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int line[100010];
int sum[100010];
int n, q, k;

int main()
{
	read(n);read(q);read(k);
	for(int i=1; i<=n; ++i)
		read(line[i]);
	line[0]=0;line[n+1]=k+1;
	
	for(int i=1; i<=n; ++i)
	{
		sum[i] += sum[i-1];
		sum[i] += (line[i] - line[i-1] - 1);
		sum[i] += (line[i+1] - line[i] - 1);
	}

	int l, r, ans;
	for(int i=1; i<=q; ++i)
	{
		read(l);read(r);
		ans = sum[r-1]-sum[l];

		ans += ((line[l] - 0 - 1)+(line[l+1]-line[l]-1));
		ans += ((line[r] - line[r-1] - 1)+(k+1 - line[r] - 1));

		printf("%d\n", ans);
	}

	return 0;
}
