#include<cstdio>//uncle-lu 701 d
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int map[510][510];
int n, m;

int main()
{
	read(n);read(m);
	for(int i=1; i<=n; ++i)
		for(int j=1; j<=m; ++j)
			read(map[i][j]);

	for(int i=1; i<=n; ++i)
	{
		for(int j=1; j<=m; ++j)
		{
			if((i+j)%2 == 1)
				printf("%d ", 720720);
			else
				printf("%d ", 720720+map[i][j]*map[i][j]*map[i][j]*map[i][j]);
		}
		printf("\n");
	}

	return 0;
}
