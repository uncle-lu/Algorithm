#include<bits/stdc++.h>
using namespace std;

class Solution {
public:

	vector<long long int>temp;

	int merge_sort(vector<int>& nums,int left,int right)
	{
		if(left >= right)return 0;
		int mid = (left + right)>>1;

		int ans=0;
		ans = merge_sort(nums,left,mid) + merge_sort(nums,mid+1,right);

		for(int i = left;i <= right;++i)
			temp[i] = nums[i];


		int i1=left,i2=mid+1;
		while(i1 <= mid && i2 <= right)
		{
			if(temp[i1] > temp[i2]*2)
			{
				ans += (mid-i1+1);
				i2++;
			}
			else i1++;
		}

		i1=left,i2=mid+1;
		for(int curr = left; curr <= right; ++curr)
			if(i1==mid+1)
				nums[curr] = temp[i2++];
			else if(i2 > right)
				nums[curr] = temp[i1++];
			else if(temp[i1] < temp[i2])
				nums[curr] = temp[i1++];
			else
				nums[curr] = temp[i2++];

		return ans;
	}

    int reversePairs(vector<int>& nums) {
		int len = nums.size();

		temp.resize(len+1);

		int ans = merge_sort(nums,0,len-1);

		return ans;
    }
};

void trimLeftTrailingSpaces(string &input) {
    input.erase(input.begin(), find_if(input.begin(), input.end(), [](int ch) {
        return !isspace(ch);
    }));
}

void trimRightTrailingSpaces(string &input) {
    input.erase(find_if(input.rbegin(), input.rend(), [](int ch) {
        return !isspace(ch);
    }).base(), input.end());
}

vector<int> stringToIntegerVector(string input) {
    vector<int> output;
    trimLeftTrailingSpaces(input);
    trimRightTrailingSpaces(input);
    input = input.substr(1, input.length() - 2);
    stringstream ss;
    ss.str(input);
    string item;
    char delim = ',';
    while (getline(ss, item, delim)) {
        output.push_back(stoi(item));
    }
    return output;
}

int main() {
	freopen("in","r",stdin);
    string line;
    while (getline(cin, line)) {
        vector<int> nums = stringToIntegerVector(line);
        
        int ret = Solution().reversePairs(nums);

        string out = to_string(ret);
        cout << out << endl;
    }
	fclose(stdin);
    return 0;
}
