#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int main()
{
	int T, a, b, c;
	read(T);
	for(int t=1;t<=T;++t)
	{
		read(a);read(b);read(c);
		if(a<b)
			std::swap(a, b);
		if(a<c)
			std::swap(a, c);
		if(b<c)
			std::swap(b, c);
		
		if(b+c <= a)
		{
			printf("%d\n",b+c);
		}
		else if(a == b)
		{
			printf("%d\n",a+c/2);
		}
		else
		{
			printf("%d\n",a);
		}
	}
	return 0;
}
