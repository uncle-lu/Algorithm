#include<cstdio>//uncle-lu
#include<algorithm>

int n, a, b, ans = 0x7f7f7f7f;
int line[210];
bool visit[210];
bool flag;

void DFS(int x, int cnt)
{
	if(ans < cnt)return ;

	if(x == b)
	{
		flag = true;
		ans = std::min(ans, cnt);
		return ;
	}

	if( x + line[x] <= n && (!visit[x+line[x]]) )
	{
		visit[x+line[x]] = true;
		DFS(x+line[x], cnt+1);
		visit[x+line[x]] = false;
	}
	
	if( x - line[x] > 0 && (!visit[x-line[x]]) )
	{
		visit[x-line[x]] = true;
		DFS(x-line[x], cnt+1);
		visit[x-line[x]] = false;
	}

	return ;
}

int main()
{
	scanf("%d %d %d", &n, &a, &b);
	for (int i = 1; i <= n; i++) 
	{
		scanf("%d", &line[i]);
	}

	visit[a] = true;
	flag = false;
	DFS(a, 0);

	if(flag)
		printf("%d", ans);
	else
		printf("-1");

	return 0;
}
