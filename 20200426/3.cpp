#include<cstdio>//uncle-lu

int n, k;
char map[10][10];
bool visit[10];
int ans;

void DFS(int x, int last)
{
	if(last == 0)
	{
		ans++;
		return ;
	}
	if(x > n)
		return ;

	for (int i = 1; i <= n; i++) 
	{
		if(map[x][i]=='.' || visit[i])continue;
		visit[i] = true;
		DFS(x+1, last-1);
		visit[i] = false;
	}

	DFS(x+1, last);

	return ;
}

int main()
{
	while(true)
	{
		scanf("%d %d\n", &n, &k);
		if(n==-1)break;

		for (int i = 1; i <= n; i++) 
			scanf("%s",map[i]+1);

		ans = 0;
		DFS(1, k);

		printf("%d\n",ans);
	}

	return 0;
}
