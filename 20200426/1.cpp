#include<cstdio>//uncle-lu
#include<queue>

/*
 * node : 每一次BFS的节点(状态)
 * x : 位置
 * t : 时间
 * visit : 标记 记录 x 位置是否搜索过
 */
struct node{
	int x, t;
};
std::queue<node>qu;
bool visit[100020];
int n, k, cnt;

int main()
{
	scanf("%d %d", &n, &k);

	if(k<n)
	{
		printf("%d",n-k);
		return 0;
    }

	visit[n] = true;
	node now=(node){n,0};
	qu.push(now);

	while(!qu.empty())
	{
		now = qu.front();
		qu.pop();
		if(now.x == k)
		{
			printf("%d",now.t);
			break;
		}

		if( now.x+1<=100010 && !visit[now.x+1])
		{
			visit[now.x+1] = true;
			qu.push((node){now.x+1, now.t+1});
		}
		if( now.x-1 >= 0 && (!visit[now.x-1]))
		{
			visit[now.x-1] = true;
			qu.push((node){now.x-1, now.t+1});
		}
		if( now.x*2 <= 100010 && (!visit[now.x*2]))
		{
			visit[now.x*2] = true;
			qu.push((node){now.x*2, now.t+1});
		}
	}

	return 0;
}
