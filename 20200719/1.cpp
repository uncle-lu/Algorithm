#include<cstdio>
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m;

int main()
{
	int T;
	read(T);
	for (int t = 1; t <= T; t++) 
	{
		read(n);read(m);
		int x, y;
		int ans = 0x7f7f7f7f;
		for (int i = 1; i <= n; i++) 
		{
			read(x);read(y);
			if(ceil((double)m/x) * y < ans)
				ans = ceil((double)m/x) * y;
		}
		printf("%d\n",ans);
	}
	return 0;
}
