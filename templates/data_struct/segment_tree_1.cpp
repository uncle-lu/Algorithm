#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void Build_Tree(int, int, int);
void Add_Tree(int, const int, const int, int, int, int);
long long int Find_Tree(int, const int, const int, int, int);
void push_down(int, int, int);

long long int Tree[300010], Lazy[300010], line[100010];
int n, m;

void Build_Tree(int step, int x, int y)
{
	if(x==y) { Tree[step] = line[x]; return ; }
	int mid = (x+y) >> 1;
	Build_Tree(step<<1, x, mid);
	Build_Tree(step<<1|1, mid+1, y);
	Tree[step] = Tree[step<<1] + Tree[step<<1|1];
	return ;
}

void push_down(int step, int x, int y)
{
	if(Lazy[step]!=0)
	{
		int mid = (x+y)>>1;
		Tree[step<<1] += Lazy[step] * (mid-x+1); Tree[step<<1|1] += Lazy[step] * (y-mid);
		Lazy[step<<1]  += Lazy[step]; Lazy[step<<1|1] += Lazy[step];
		Lazy[step] = 0;
	}
	return ;
}

void Add_Tree(int step, const int l, const int r, int x, int y, int val)
{
	if( l<=x && y<=r )
	{
		Tree[step] += val*(y-x+1);
		Lazy[step] += val;
		return ;
	}
	push_down(step, x, y);
	int mid = (x+y)>>1;
	if(mid+1<=r)
		Add_Tree(step<<1|1, l, r, mid+1, y, val);
	if(l<=mid)
		Add_Tree(step<<1, l, r, x, mid, val);
	Tree[step] = Tree[step<<1] + Tree[step<<1|1];
	return ;
}

long long int Find_Tree(int step, const int l, const int r, int x, int y)
{
	if( l<=x && y<=r )
		return Tree[step];
	push_down(step, x, y);
	int mid = (x+y)>>1;
	long long int sum=0;
	if(mid+1<=r)
		sum+=Find_Tree(step<<1|1,l,r,mid+1,y);
	if(l<=mid)
		sum+=Find_Tree(step<<1,l,r,x,mid);
	return sum;
}

int main()
{
	read(n);read(m);
	for(int i=1;i<=n;++i)
		read(line[i]);
	Build_Tree(1,1,n);
	int temp,x,y,k;
	for(int i=1;i<=m;++i)
	{
		read(temp);
		if(temp == 1) // 1  区间加
		{
			read(x);read(y);read(k);
			Add_Tree(1,x,y,1,n,k);
		}
		else if(temp == 2)// 2 区间查询
		{
			read(x);read(y);
			printf("%lld\n",Find_Tree(1,x,y,1,n));
		}
	}
	return 0;
}
