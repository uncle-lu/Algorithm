#include<cstdio>//uncle-lu
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;bool f=false;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x = (x<<1) + (x<<3) + (ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void ST_prework();
int ST_find(int, int);

int n, m;
int F[100010][20];
int line[100010];

void ST_prework()
{
	for(int i=1;i<=n;++i)
		F[i][0] = line[i];

	int t = log(n)/log(2);

	for(int j=1;j<=t;++j)
		for(int i=1; i<=n-(1<<j)+1; ++i)
			F[i][j]=std::max(F[i][j-1], F[i+(1<<(j-1))][j-1]);

	return ;
}

int ST_find(int x,int y)
{
	int t = log(y-x+1)/log(2);
	return std::max(F[x][t], F[y-(1<<t)+1][t]);
}

int main()
{
	read(n);read(m);
	for(int i=1; i<=n; ++i)
		read(line[i]);
	ST_prework();
	int a, b;
	for(int i=1; i<=m; ++i)
	{
		read(a);read(b);
		printf("%d\n",ST_find(a, b));
	}
	return 0;
}
