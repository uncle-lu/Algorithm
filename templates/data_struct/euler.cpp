#include<cstdio>//uncle-lu luogu.org P3383
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}
void Euler(void);
int n,m;
int P[10000010], cnt;
bool flag[10000010];
void Euler()
{
	flag[1]=true;
	for(int i=2;i<=n;++i)
	{
		if(!flag[i])P[++cnt] = i;
		for(int j=1;P[j]*i<=n && j<=cnt;++j)
		{
			flag[P[j]*i] = true;
			if(!(i%P[j]))break;
		}
	}
	return ;
}
int main()
{
	read(n);read(m);
	Euler();
	int temp;
	for(int i=1;i<=m;++i)
	{
		read(temp);
		if(!flag[temp])printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}
