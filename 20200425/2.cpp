#include<cstdio> //uncle-lu
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int N = 500010;
int n;
int line[N];
int F[N][20], G[N][20];
int list[N];

int gcd(int a, int b)
{
	return b ? gcd(b, a%b) : a;
}

void pre_STtable()
{
	for (int i = 1; i <= n; i++) 
	{
		F[i][0] = line[i];
		G[i][0] = line[i];
	}
	int t=log(n)/log(2) + 1;
	for (int j = 1; j < t; j++) 
		for (int i = 1; i <= n - (1<<j) + 1; i++) 
		{
			F[i][j] = std::min(F[i][j-1], F[i + ( 1 << (j-1) )][j-1]);
			G[i][j] = gcd(G[i][j-1], G[i + (1 << (j-1) )][j-1]);
		}

	return ;
}

bool check(int x)
{
	int t = log(x) / log(2);
	for (int i = 1; i <= n-x+1; i++) 
	{
		int mi = std::min(F[i][t], F[i - (1<<t) + 1][t]);
		int gd = gcd(G[i][t], G[i - (1<<t) + 1][t]);
		if(mi == gd)return true;
	}

	return false;
}

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
		read(line[i]);

	pre_STtable();

	int l = 0, r = n, mid, ans;
	while(l<=r)
	{
		mid = (l + r) >> 1;
		if( check(mid) )
		{
			ans = l;
			l = mid+1;
		}
		else
			r = mid-1;
	}

	int cnt = 0;
	int t = log(ans) / log(2);
	for(int i = 1; i <= n-ans+1; i++)
	{
		int mi = std::min(F[i][t], F[i - (1<<t) + 1][t]);
		int gd = gcd(G[i][t], G[i - (1<<t) + 1][t]);
		if(mi == gd)list[++cnt] = i;
	}

	printf("%d %d\n", cnt, ans);
	for(int i=1;i<=cnt;++i)
		printf("%d ", list[i]);

	return 0;
}
