#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
const int maxn=2e5+5;

int change[maxn];
int n,m,q;
int x[maxn],y[maxn],c[maxn*2];
int a[maxn];
int cnt=0,ans[maxn];
int main()
{
	memset(change, 0, sizeof(change));
	scanf("%d %d %d",&n, &m, &q);
	// 将x y a 出现的数字均放到c数组内
	for(int i=1; i<=m; ++i)
	{
		scanf("%d %d", &x[i], &y[i]);
		c[++cnt]=x[i],c[++cnt]=y[i];
	}
	for(int i=1; i<=q; ++i)
	{
		scanf("%d", &a[i]);
		c[++cnt]=a[i];
	}
	std::sort(c+1,c+cnt+1);//将c排序
	cnt=std::unique(c+1,c+cnt+1)-c-1;//去重复出现的数字
	for(int i=1;i<=m;i++)
	{
		x[i]=std::lower_bound(c+1,c+cnt+1,x[i])-c;
		y[i]=std::lower_bound(c+1,c+cnt+1,y[i])-c;
		change[x[i]]++;
		change[y[i]+1]--;
	}
	for(int i=1; i<=cnt; ++i)
		change[i] += change[i-1];
	int temp;
	for(int i=1;i<=q;i++)
	{
		temp=a[i];
		temp=std::lower_bound(c+1,c+cnt+1,temp)-c;
		printf("%d\n",change[temp]);
	}
	return 0;
}
