#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, a, b, cnt;
int line[20];
int left[20], right[20];
int visit[20];

void DFS(int x)
{
	if(x == n+1)
	{
		int temp = 0;
		for (int i = 1; i <= n; i++) 
			temp += visit[i];

		if(temp <= b && temp >= a)
			cnt++;
	}

	for (int i = left[x]; i <= right[x]; i++) 
	{
		visit[x] = i;
		DFS(x+1);
	}

	return ;
}

int main()
{
	read(n); read(a); read(b);

	for (int i = 1; i <= n; i++) 
		read(line[i]);

	for (int i = 1; i <= n; i++) 
	{
		read(left[i]); read(right[i]);
	}

	DFS(1);

	printf("%d", cnt);

	return 0;
}
