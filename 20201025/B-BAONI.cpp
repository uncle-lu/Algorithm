#define method_1
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#include<cstring>
#include<cstdlib>
#include<iomanip>
#include<ctime>
#include<string>
#include<bitset>
#define D(x) cout<<#x<<" = "<<x<<"  "
#define E cout<<endl
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rep0(i,s,t) for(int i=(s);i<(t);i++)
#define rrep(i,t,s) for(int i=(t);i>=(s);i--)
using namespace std;
typedef long long ll;
typedef pair<double,int>pii;
const int maxn=302*2+5;
const int maxm=604*604*2+5;
const int INF=0x3f3f3f3f;
int n,m,k;
int cnt=0;
struct Point{
    double x,y;
    int id;
    Point(){}
    Point(double a,double b){x=a;y=b;}
}p[maxn];
struct Line{
    Point a,b;
    Line(){}
    Line(Point x,Point y){a=x;b=y;}
}l[maxn];
int head[maxn],tot=1;
struct node{
	int from,to;
	double w;
}edge[maxm];
double calc(int x,int y){
	return sqrt(double(p[x].x-p[y].x)*(p[x].x-p[y].x)+double(p[x].y-p[y].y)*(p[x].y-p[y].y));
}
void add(int from,int to){
	edge[++tot].from=head[from],head[from]=tot,edge[tot].to=to,edge[tot].w=calc(from,to);
}
bool judge(Point &a,Point &b,Point &c,Point &d){
    if(!(min(a.x,b.x)<=max(c.x,d.x) && min(c.y,d.y)<=max(a.y,b.y)&&min(c.x,d.x)<=max(a.x,b.x) && min(a.y,b.y)<=max(c.y,d.y)))
       return false;
    double u,v,w,z;
    u=(c.x-a.x)*(b.y-a.y)-(b.x-a.x)*(c.y-a.y);
    v=(d.x-a.x)*(b.y-a.y)-(b.x-a.x)*(d.y-a.y);
    w=(a.x-c.x)*(d.y-c.y)-(d.x-c.x)*(a.y-c.y);
    z=(b.x-c.x)*(d.y-c.y)-(d.x-c.x)*(b.y-c.y);
    return (u*v<=0.00000001 && w*z<=0.00000001);
}
priority_queue<pii>q;
double d[maxn];
int v[maxn];
void dijkstra(){
	q.push(make_pair(0.0,0));
	rep(i,1,cnt) d[i]=1e18;
	while(q.size()){
		int x=q.top().second;q.pop();
		if(v[x]) continue;
		v[x]=1;
		for(int i=head[x];i;i=edge[i].from){
			int y=edge[i].to;double w=edge[i].w;
			if(d[y]>d[x]+w){d[y]=d[x]+w;q.push(make_pair(-d[y],y));}
		}
	}
}
void build(){
	rep(i,0,cnt) rep(j,i+1,cnt){
		if(p[i].id==p[j].id){add(i,j);add(j,i);continue;}
		bool f=true;
		rep(t,1,k){
			if(t==p[i].id||t==p[j].id) continue;
//			if(i==2&&j==5){D(p[i].x);D(p[i].y);D(p[j].x);D(p[j].y);D(l[t].a.x);D(l[t].a.y);D(l[t].b.x);D(l[t].b.y);}
			if(judge(p[i],p[j],l[t].a,l[t].b)){f=false;break;}
		}
		if(f){add(i,j);add(j,i);}
	}
}
void solve() {
	build();
	dijkstra();
	printf("%.4lf",d[cnt]);
}
int main() {
//	ios::sync_with_stdio(false);
//	freopen("B.in","r",stdin);
	scanf("%d%d%d",&n,&m,&k);
	rep(i,1,k){
		++cnt;scanf("%lf%lf",&p[cnt].x,&p[cnt].y);
		++cnt;scanf("%lf%lf",&p[cnt].x,&p[cnt].y);
		p[cnt].id=p[cnt-1].id=i;
		l[i].a=p[cnt-1],l[i].b=p[cnt];
	}
//	rep(i,1,cnt){D(p[i].x);D(p[i].y);E;}
	++cnt;scanf("%lf%lf%lf%lf",&p[0].x,&p[0].y,&p[cnt].x,&p[cnt].y);
	p[0].id=0,p[cnt].id=k+1;
	solve();
	return 0;
}
