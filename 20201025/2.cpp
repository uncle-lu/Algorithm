#include<cstdio>//BAONI
#include<cstring>
#include<algorithm>
using namespace std;
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int v,next;
}edge[200010];
int head[100010], Low[100010], DFN[100010], lis[100010];
int n,m,cnt,tot;
bool iscut[100010];
int ans;

void add_edge(int x,int y)
{
	edge[cnt].v=y;
	edge[cnt].next=head[x];
	head[x]=cnt;
	cnt++;
	return ;
}

void DFS(int x,int fa)
{
	Low[x]=DFN[x]=++tot;
	int child=0;
	for(int i=head[x];~i;i=edge[i].next)
	{
		if(!DFN[edge[i].v])
		{
			DFS(edge[i].v,x);
			child++;
			Low[x]=min(Low[edge[i].v],Low[x]);
			if(Low[edge[i].v]>=DFN[x])
			{
				iscut[x]=true;
			}
		}
		else if(fa!=edge[i].v && DFN[edge[i].v]<=DFN[x])
		{
			Low[x]=min(DFN[edge[i].v],Low[x]);
		}
	}
	if(child==1&&x==fa)
	{
		iscut[x]=false;
	}

	return ;
}

int main()
{
	//freopen("in","r",stdin);
	memset(head,0xff,sizeof(head));
	int a,b;
	read(n);read(m);
	for(int i=1;i<=m;++i)
	{
		read(a);read(b);
		add_edge(a,b);
		add_edge(b,a);
	}
	for(int i=1;i<=n;++i)
	{
		if(!DFN[i]){
			DFS(i,i);
		}
	}
	for(int i=1;i<=n;++i)
	{
		if(iscut[i])
		{
			ans++;
			lis[ans]=i;
		}
	}
	printf("%d\n",ans);
	for(int i=1;i<=ans;++i)printf("%d ",lis[i]);
	//fclose(stdin);
	return 0;
}
