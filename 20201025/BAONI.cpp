#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int map[1000][1000];
int n, m, a, b;

bool check(int x, int y)
{
	if(x > n-a+1)
		return false;
	if(y > m-b+1)
		return false;

	int t = map[x][y];
	for(int i = x; i <= x+a-1 && i <= n; ++i)
	{
		for (int j = y; j <= y+b-1 && j <= m; j++) 
		{
			if(t > map[i][j])
				return false;
			map[i][j] -= t;
		}
	}

	return true;
}

int main()
{
	int T;
	read(T);
	for (int t = 1; t <= T; t++) 
	{
		read(n); read(m); read(a); read(b);
		for (int i = 1; i <= n; i++) 
			for (int j = 1; j <= m; j++) 
				read(map[i][j]);

		bool flag = true;
		for (int i = 1; i <= n; i++) 
		{
			for (int j = 1; j <= m; j++) 
			{
				if(map[i][j]!=0)
				{
					if(!check(i, j))
					{
						flag = false;
						break;
					}
				}
			}
			if(!flag)break;
		}

		if(!flag)
			printf("QAQ\n");
		else
			printf("^_^\n");
	}
}
