/*

*/
#define method_1
#ifdef method_1
/*

*/
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#include<cstring>
#include<cstdlib>
#include<iomanip>
#include<ctime>
#include<string>
#include<bitset>
#define D(x) cout<<#x<<" = "<<x<<"  "
#define E cout<<endl
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rep0(i,s,t) for(int i=(s);i<(t);i++)
#define rrep(i,t,s) for(int i=(t);i>=(s);i--)
using namespace std;
typedef long long ll;
typedef pair<int,int>pii;
const int maxn=1000+5;
const int INF=0x3f3f3f3f;
int T;
int n,m,a,b;
ll M[maxn][maxn];
ll c[maxn][maxn];
inline int lowbit(int x) {
	return x&-x;
}
void add(int x,int y,ll v) {
	while(x<=n) {
		int ty=y;
		while(ty<=m)
			c[x][ty]+=v,ty+=lowbit(ty);
		x+=lowbit(x);
	}
}
void real_add(int x1,int y1,int x2,int y2,ll v) {
	add(x1,y1,v);
	add(x1,y2+1,-v);
	add(x2+1,y1,-v);
	add(x2+1,y2+1,v);
}
ll ask(int x, int y) {
	ll res=0;
	while(x) {
		int ty=y;
		while(ty)
			res+=c[x][ty],ty-=lowbit(ty);
		x-=lowbit(x);
	}
	return res;
}
void show(){
	rep(i,1,n){
		rep(j,1,m){
			ll res=ask(i,j);
			printf("%lld ",res);
		}
		E;
	}
}
void solve() {
	memset(c,0,sizeof(c));
	rep(i,1,n) rep(j,1,m) {
		if(!M[i][j]) continue;
		real_add(i,j,i,j,M[i][j]);
	}
//	show();
	rep(i,1,n) rep(j,1,m) {
		if(i+a-1>n||j+b-1>m) continue;
		ll res=ask(i,j);
//		D(res);
		if(!res) continue;
		if(res<0){
			printf("QAQ\n");
			return;
		}
		real_add(i,j,i+a-1,j+b-1,-res);
//		show();
	}
	rep(i,1,n) rep(j,1,m) {
		ll res=ask(i,j);
		if(res!=0ll) {
			printf("QAQ\n");
			return;
		}
	}
	printf("^_^\n");
}
int main() {
//	ios::sync_with_stdio(false);
	//freopen("J.in","r",stdin);
	scanf("%d",&T);
	while(T--) {
		scanf("%d%d%d%d",&n,&m,&a,&b);
		rep(i,1,n) rep(j,1,m) scanf("%lld",&M[i][j]);
		solve();
	}
	return 0;
}
#endif
#ifdef method_2
/*

*/

#endif
#ifdef method_3
/*

*/

#endif

