#include<cstdio>//uncle-lu
#include<cstring>
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void add_edge(int, int, int);
void init(void);
void spfa(void);

struct node{
	int v, next, val;
};
node edge[50010];
int head[50010], cnt, n;
int d[50010];
bool visit[50010];

void init()
{
	memset(visit,false,sizeof(visit));
	memset(head,-1,sizeof(head));
	cnt = 0;

	return ;
}

void add_edge(int x, int y, int val)
{
	edge[++cnt].v = y;
	edge[cnt].next=head[x];
	edge[cnt].val = val;
	head[x] = cnt;
}

void spfa()
{
	memset(d,0x7f7f7f7f,sizeof(d));
	visit[0] = true;
	d[0] = 0;

	std::queue<int>qu;
	qu.push(0);
	while(!qu.empty())
	{
		int now = qu.front();
		qu.pop();
		visit[now] = false;
		
		for(int i=head[now];~i;i=edge[i].next)
		{
			if(d[edge[i].v] > d[now] + edge[i].val)
			{
				d[edge[i].v] = d[now] + edge[i].val;
				if(!visit[edge[i].v])
				{
					visit[edge[i].v] = true;
					qu.push(edge[i].v);
				}
			}
		}
	}

	return ;
}

int main()
{
	int T, x, y, c;
	read(T);
	for(int t=1;t<=T;++t)
	{
		init();
		read(n);
		for(int i=1;i<=n;++i)
		{
			read(x); read(y); read(c);
			add_edge(x-1, y, c);
		}
		for(int i=1;i<=50000;++i)
		{
			add_edge(i, i-1, -1);
			add_edge(i-1, i, 0);
		}

		spfa();

		printf("%d",d[50000]);

	}
	return 0;
}
