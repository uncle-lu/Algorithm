#include<cstdio>//uncle-lu
#include<cstring>
#include<queue>
#include<algorithm>

const int Mod = 80112002;

struct node{
	int v,next;
};
node edge[500010];
int head[5010];
int r[5010], c[5010];
int f[5010];
int n, cnt, m;

void add_edge(int x, int y)
{
	edge[++cnt].v = y;
	edge[cnt].next = head[x];
	head[x] = cnt;
	return ;
}

int main()
{
	memset(head, -1, sizeof(head));

	int a, b;
	scanf("%d %d", &n, &m);
	for(int i=1; i<=m; i++)
	{
		scanf("%d %d", &a, &b);
		add_edge(a, b);
		r[b]++;c[a]++;
	}

	std::queue<int> qu;
	for(int i=1; i<=n; ++i)
		if(r[i]==0)
		{
			qu.push(i);
			f[i]=1;
		}

	int ans = 0;
	while(!qu.empty())
	{
		int now = qu.front();
		qu.pop();

		for(int i=head[now]; ~i; i=edge[i].next)
		{
			f[edge[i].v]+=f[now];
			f[edge[i].v]%=Mod;
			r[edge[i].v]--;
			if(r[edge[i].v] == 0)
			{
				qu.push(edge[i].v);
				if(c[edge[i].v] == 0)
				{
					ans += f[edge[i].v];
					ans %= Mod;
				}
			}
		}
	}

	printf("%d",ans);
	return 0;
}
