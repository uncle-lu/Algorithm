#include <cstdio>//uncle-lu
#include <algorithm>

const int INF = 2010;

struct pos{
	int x,y;
};
struct node{
	int x,y,val;
	friend bool operator<(const node a, const node b)
	{
		return a.val < b.val;
	}
};

pos line[INF];
node edge[INF*INF];
int n,c;
int Father[INF];

int Find_Father(int x)
{
	return Father[x] == x ? x : Father[x] = Find_Father(Father[x]);
}

int main()
{
	scanf("%d %d", &n, &c);
	for(int i=1;i<=n;++i)
		scanf("%d %d", &line[i].x, &line[i].y);

	int m = 0;
	for(int i=1;i<=n;++i)
	{
		for(int j=i+1;j<=n;++j)
		{
			m++;
			edge[m].x = i;
			edge[m].y = j;
			edge[m].val = (line[i].x - line[j].x)*(line[i].x - line[j].x) + (line[i].y - line[j].y)*(line[i].y - line[j].y);
		}
	}

	for(int i=1; i<=n; ++i) Father[i] = i;
	std::sort(edge+1,edge+1+m);

	int cnt = 0, ans = 0;
	for(int i=1;i<=m;++i)
	{
		if(edge[i].val < c)continue;
		int xx = Find_Father(edge[i].x), yy = Find_Father(edge[i].y);
		if(xx != yy)
		{
			Father[yy] = xx;
			ans += edge[i].val;
			cnt++;
		}
		if(cnt == n-1)break;
	}

	if(cnt!=n-1)
		printf("-1");
	else
		printf("%d",ans);

	return 0;
}
