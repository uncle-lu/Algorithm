#include <cstdio>//uncle-lu
#include <algorithm>

const int INF = 100010;

struct node{
	int x, y, val;
	friend bool operator<(const node a, const node b)
	{
		return a.val < b.val;
	}
};
node edge[INF];
int Father[310], n, m;

int Find_Father(int x)
{
	return Father[x] == x ? x : Father[x] = Find_Father(Father[x]);
}

int main()
{
	scanf("%d %d", &n, &m);
	for(int i=1;i<=n;++i) Father[i] = i;
	for(int i=1; i<=m; ++i)
	{
		scanf("%d %d %d", &edge[i].x, &edge[i].y, &edge[i].val);
	}

	std::sort(edge+1,edge+1+m);

	int cnt=0,ans = 0;
	for(int i=1;i<=m;++i)
	{
		int xx = Find_Father(edge[i].x), yy = Find_Father(edge[i].y);
		if(xx != yy)
		{
			Father[yy] = xx;
			ans = edge[i].val;
			cnt++;
		}
		if(cnt == n-1)break;
	}
	printf("%d %d",n-1, ans);
	return 0;
}
