#include<cstdio>//uncle-lu
#include<algorithm>

bool visit[20];
int n;
char flag;

int main()
{
	scanf("%d\n",&n);
	for(int i=1;i<=n;++i)
	{
		scanf("%c",&flag);
		if(flag=='L')
		{
			for(int j=0;j<10;++j)
			{
				if(!visit[j])
				{
					visit[j] = true;
					break;
				}
			}
		}
		else if(flag=='R')
		{
			for(int j=9;j>=0;--j)
			{
				if(!visit[j])
				{
					visit[j] = true;
					break;
				}
			}
		}
		else
		{
			int k = flag - '0';
			visit[k] = false;
		}
	}
	
	for(int j=0;j<10;++j)
	{
		if(visit[j])printf("1");
		else printf("0");
	}
	return 0;
}
