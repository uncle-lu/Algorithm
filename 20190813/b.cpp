#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int cases;
int n,bag,k;
int line[110];

int main()
{
	//freopen("in","r",stdin);

	read(cases);
	for(int t=1;t<=cases;t++)
	{
		memset(line,0,sizeof(line));
		read(n);read(bag);read(k);
		for(int i=1;i<=n;++i)
			read(line[i]);

		bool flag=true;
		for(int i=1;i<n;++i)
		{
			if(line[i+1] - line[i] >= k)
			{
				int need = line[i+1] - k - line[i];
				if(bag < need)
				{
					flag = false;
					break;
				}
				else
					bag -= need;
			}
			else if(line[i] - line[i+1] >= k)
			{
				int need = line[i+1] - k <= 0 ? 0 : line[i+1] - k;
				need = line[i] - need;
				bag += need;
			}
			else 
			{
				int h = line[i+1] - k <= 0 ? 0 : line[i+1] - k;
				if(line[i] >= h)
					bag += line[i] - h;
				else
				{
					int need = h - line[i];
					if(bag < need)
					{
						flag = false;
						break;
					}
					else
						bag -= need;
				}
			}
		}

		if(flag)printf("YES\n");
		else printf("NO\n");
	}

	//fclose(stdin);
	return 0;
}
