#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
    x=0;char f=false;char ch=getchar();
    while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
    while(ch<='9' && ch>='0') { x = (x<<1) + (x<<3) + (ch^48); ch = getchar(); }
    x = f ? -x : x;
    return ;
}

const int CEIL = 50010;

void add_edge(int, int);

struct node{
    int v,next;
};
node edge[CEIL * 2];
int head[CEIL], cnt, Father[CEIL][20], M[CEIL][20], m[CEIL][20], DP[CEIL][20];
int line[CEIL];
int n;

void add_edge(int x, int y)
{
    edge[++cnt].v = y;
    edge[cnt].next = head[x];
    head[x] = cnt;
    return ;
}

int main()
{
    read(n);

    return 0;
}