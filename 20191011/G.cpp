#include<cstdio>//uncle-lu
#include<cstring>
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void init(void);
void pre_str(void);
void pre_st_table(void);
int find_min(int, int);

int n,m;
char line[100010];
int sum[100010];
int F[100010][23];

void init()
{
	memset(F,0,sizeof(F));
	memset(sum,0,sizeof(sum));
	return;
}

void pre_str()
{
	for(int i=1;i<=n;++i)
	{
		if(line[i]=='(')
			sum[i] = 1;
		else
			sum[i] = -1;
		sum[i] = sum[i-1] + sum[i];
	}
	return ;
}

void pre_st_table()
{
	for(int i=1;i<=n;++i)
		F[i][0] = sum[i];

	int t = log(n) / log(2);

	for(int j=1;j<=t;++j)
		for(int i=1;i<=n - (1<<j) + 1;++i)
			F[i][j] = std::min(F[i][j-1],F[i+(1<<(j-1))][j-1]);

	return ;
}

int find_min(int x, int y)
{
	int k = log(y-x+1)/log(2);
	return std::min(F[x][k],F[y-(1<<k)+1][k]);
}

int main()
{
	while(~scanf("%d %d",&n,&m))
	{
		init();

		scanf("%s",line+1);

		pre_str();
		pre_st_table();

		int x,y;
		for(int i=1;i<=m;++i)
		{
			read(x);read(y);
			if(x>y)std::swap(x, y);
			if(line[x]=='(' && line[y]==')')
			{
				if(find_min(x,y-1) >= 2)
					printf("Yes\n");
				else
					printf("No\n");
			}
			else
				printf("Yes\n");
		}

	}
	return 0;
}
