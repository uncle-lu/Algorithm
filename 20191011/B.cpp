#include<cstdio>//uncle-lu
#include<cstring>
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 0x7f7f7f7f;
const int MOD = 1e9 + 7;

void init(void);
void add_edge(int, int);
void BFS(void);

struct node{
	int v,next;
};
node edge[100010];
int head[100010], cnt;

std::queue<int>qu;
long long int a[100010], b[100010], ans, tong[100010];
int d[100010];
int n,m;

void init()
{
	memset(d,0,sizeof(d));
	memset(head,-1,sizeof(head));
	memset(tong,0,sizeof(tong));
	cnt=0;
	ans=0;
	return ;
}

void add_edge(int x, int y)
{
	d[y] ++;
	edge[++cnt].v=y;
	edge[cnt].next=head[x];
	head[x] = cnt;
	return ;
}

void BFS()
{
	for(int i=1;i<=n;++i)
		if(!d[i])qu.push(i);

	while(!qu.empty())
	{
		int top = qu.front();
		qu.pop();
		for(int i=head[top];~i;i=edge[i].next)
		{
			d[edge[i].v]--;
			if(!d[edge[i].v])qu.push(edge[i].v);
			tong[edge[i].v] += a[top];
			tong[edge[i].v] += tong[top];
			tong[edge[i].v] %= MOD;
		}

		ans += (b[top] * tong[top]) % MOD;
		ans %= MOD;
	}
	return ;
}

int main()
{
	while(~scanf("%d%d",&n,&m))
	{
		init();

		for(int i=1;i<=n;++i) { read(a[i]);read(b[i]); }

		int x,y;
		for(int i=1;i<=m;++i)
		{
			read(x);read(y);
			add_edge(x,y);
		}

		BFS();

		printf("%lld\n",ans);
	}
	return 0;
}
