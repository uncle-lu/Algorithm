#include<cstdio>//uncle-lu
#include<algorithm>

struct node{
	/*
	 *  x : x坐标
	 *  y : y坐标
	 *  h : 高度
	 *  w : 宽度
	 */
	long long int x,y,h,w;
}a[10010];
long long int tot;
long long int r,n;

/* sum函数 
 * 作用:
 *		统计分割线左边的面积和
 * 变量:
 *		x : 分割线
 *		sum_all : 分割线左边所有矩阵的和
 */
long long int sum(long long int x)
{
	long long int sum_all=0;
	for(int i=1;i<=n;++i)
	{
		if(a[i].x+a[i].w<=x)
		{
			sum_all+=a[i].h*a[i].w;
		}
		else if(a[i].x<x&&a[i].x+a[i].w>x)
		{
			sum_all+=a[i].h*(x-a[i].x);
		}
		else break;
	}
	return sum_all;
}

bool cmp(node x,node y)
{
	return x.x < y.x;
}

int main()
{
	scanf("%lld",&r);
	scanf("%lld",&n);
	for(int i=1;i<=n;++i)
	{
		scanf("%lld%lld%lld%lld",&a[i].x,&a[i].y,&a[i].w,&a[i].h);
		tot+=(a[i].h*a[i].w);
	}
	std::sort(a+1,a+1+n,cmp);
	/*
	 * ans_mean : 记录答案分割线分割出的左区间大小
	 * ans : 记录答案分割线
	 */
	long long int left=0,right=r+1,mid;
	long long int ans_mean;
	long long int ans=0;
	while(left+1<right)
	{
		mid=(left+right)/2;
		long long int sum_left=sum(mid);
		if(sum_left*2>=tot)
		{
			ans_mean=sum_left;
			ans=mid;
			right=mid;
		}
		else left=mid;
	}
	/*
	 * while将分割线尽量往右移(处理存在空白空挡的部分)
	 */
	while(ans<r&&sum(ans+1)==ans_mean)ans++;
	printf("%lld\n",ans);
	return 0;
}
