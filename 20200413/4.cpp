#include<cstdio>//uncle-lu
#include<algorithm>

int x[100100],y[100010];
int n,m;

int main()
{
	scanf("%d",&n);
	for(int i=1;i<=n;++i)scanf("%d", &x[i]);
	for(int i=1;i<=n;++i)scanf("%d", &y[i]);

	std::sort(x+1,x+1+n);
	std::sort(y+1,y+1+n);

	scanf("%d",&m);
	for(int i=1;i<=m;i++)
	{
		int x_1,y_1;
		scanf("%d %d", &x_1, &y_1);

		int l=1,r=n;
		while(l<r)
		{
			int mid=(l+r+1) >> 1;
			/*
			 * 使用叉积判断线段是否相交
			 * 如果相交，枚举右区间
			 * 如果不相交，枚举左区间
			 */
			if((long long)x[mid]*y_1+(long long)y[mid]*x_1 >= (long long)x[mid]*y[mid])
				l=mid;
			else r=mid-1;
		}
		if((long long)x[l]*y_1+(long long)y[l]*x_1 >= (long long)x[l]*y[l])
			printf("%d",l);
		else printf("%d",0);
		printf("\n");
	}
	return 0;
}
