#include <cstdio>//uncle-lu
#include <algorithm>

int n, m;
int lef, rig, mid, ans;
int a[100010];

/*
 * check 函数
 * 作用:
 *		根据枚举出的最大值，来分组，根据分出的组数来调整最大值
 * 变量:
 *		x : 枚举出的最大值
 *		sum : 分组时每组的和
 *		count : 分出的组数
 */
bool check(int x)
{
    int sum=0, count=1;//t2: 组数 t1: 每组的和
    for(int i=0; i<n; i++)
	{
        if(sum+a[i]<=x)sum+=a[i];
        else sum=a[i],count++;
    }
    return count>m;
}

int main()
{
	scanf("%d %d", &n, &m);
    for(int i=0; i<n; i++)
	{
		scanf("%d", &a[i]);
        rig += a[i];
		lef = std::max(lef, a[i]);
    }

	/*
	 * 二分答案，枚举出一个最大值，根据分组情况调整最大值，求出最优最大值。
	 */

    while( lef<=rig )
	{
        mid=(lef+rig)/2;
        if( check(mid) )
			lef=mid+1;
        else 
		{
			ans=mid;
			rig=mid-1;
		}
    }

	printf("%d",ans);
    return 0;
}
