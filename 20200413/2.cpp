#include<cstdio>//uncle-lu

/*
 * line : 所有网线的长度
 * mx : 最长的网线
 */
int line[10010];
int n,k,mx;

/* count函数:
 * 作用:
 *		统计根据x长度能截出多少网线
 *
 * 变量:
 *		x : 枚举出的答案长度
 */
int count(int x)
{
	int sum = 0;
	for(int i=1;i<=n;++i)
		sum += line[i]/x;
	return sum;
}

int main()
{
	double temp;
	scanf("%d %d",&n,&k);
	for(int i=1;i<=n;++i)
	{
		scanf("%lf",&temp);
		line[i] = temp*100;// 将长度由米变成厘米
		if(line[i] > mx)mx = line[i];
	}

	/*
	 *
	 * 二分答案，根据枚举出的长度去算截出的数量，再根据数量调整长度
	 *
	 */

	int l=1,r=mx,mid,ans=0;
	while(l<=r)
	{
		mid = (l+r)/2;
		if(count(mid)<k)r = mid-1;
		else
		{
			ans = mid;
			l = mid+1;
		}
	}

	printf("%.2lf",ans/100.00);// 将长度由厘米变成米
	return 0;
}
