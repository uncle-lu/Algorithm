#include<cstdio>
#include<algorithm>

int main()
{
	int T, n;
	scanf("%d", &T);
	for (int t = 1; t <= T; t++) 
	{
		scanf("%d", &n);
		int cnt = 0;
		int temp;
		for (int i = 1; i <= n; i++) 
		{
			scanf("%d", temp);
			if(check(temp))
				cnt ++;
		}

		if( cnt%2 == 0 && n - cnt > 0)
			printf("W\n");
		else
			printf("L\n");
	}
}
