#include<cstdio>//uncle-lu
#include<algorithm>

long long int quickPow(long long int a, long long int b, long long int p) {
	long long int sum=1;
	for(; b; b>>=1,a=a*a%p)if(b&1)sum=sum*a%p;
	return sum;
}

bool millerRabbin(long long int n) {
	if (n < 3) return n == 2;
	long long int a = n - 1, b = 0;
	while (a % 2 == 0) a /= 2, ++b;
	int test_time = 8;
	// test_time 为测试次数,建议设为不小于 8
	// 的整数以保证正确率,但也不宜过大,否则会影响效率
	for (int i = 1, j; i <= test_time; ++i) {
		long long int x = rand() % (n - 2) + 2, v = quickPow(x, a, n);
		if (v == 1 || v == n - 1) continue;
		for (j = 0; j < b; ++j) {
			v = (long long)v * v % n;
			if (v == n - 1) break;
		}
		if (j >= b) return 0;
	}
	return 1;
}

int main()
{
	freopen("out","w",stdout);
	long long int sum = 0;

	printf("if(n == 1)printf(\"%%lld\\n\", %lldLL %% k);\n", sum);
	for(long long int i=2; i<=10000000000; i++)
	{
		if(millerRabbin(i+1))
			sum += (i+1)*2;
		else
			sum += (i+1);

		printf("if(n == %lld)printf(\"%%lld\\n\", %lldLL %% k);\n", i, sum);
	}

	fclose(stdout);
	return 0;
}
