#include <iostream>
#include <cstdio>

using namespace std;

int n, a[100], b[100];

int main()
{
    scanf("%d", &n);
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);
    b[n - 1] = a[n - 1];
    for (int i = n - 2; i >= 0; i--) {
        if (a[i] < b[i + 1])
            b[i] = a[i];
        else
            b[i] = b[i + 1];
    }
    int c = 0, d = 0;
    for (int i = 0; i < n; i++) {
        if (a[i] > c)
            c = a[i];
        if (c == b[i])
            d++;
    }
    cout << d << endl;
    return 0;
}
