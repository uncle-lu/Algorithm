#include <bits/stdc++.h>
#include <cstring>
#define ll long long
#define next fuck
#define fi first
#define se second
using namespace std;
const int MAXN = 6000;
bool vis[MAXN+10][MAXN+10];
int val[MAXN+10][MAXN+10];
vector<pair<int,int> > f[10000010];
vector<pair<int,int> > tot;
int dx[4] = {1,1,-1,-1};
int dy[4] = {1,-1,-1,1};
int main()
{

	for(int i  =0;i<=MAXN;i++)
	{
		for(int j = 0;j<=MAXN;j++)
		{
			if(i*i+j*j<=10000000) f[i*i+j*j].emplace_back(i,j);
			else break;

		}
	}
	int ca,cat = 1;
	scanf("%d",&ca);
	while(ca--)
	{
		int n,m;
		scanf("%d%d",&n,&m);
		tot.clear();
		int op,x,y,w,k;

		ll last = 0,ans = 0;
		for(int i = 0;i<n;i++)
		{
			scanf("%d%d%d",&x,&y,&w);
			vis[x][y] = 1;
			val[x][y] = w;
			tot.push_back(make_pair(x,y));
		}
		printf("Case #%d:\n",cat++);
		auto check = [&](int x,int y)
		{
			return x >= 1 && x<= MAXN && y >= 1 && y <= MAXN && vis[x][y] == 1;
		};
		set<pair<int,int> > s;
		while(m--)
		{

			s.clear();
			scanf("%d%d%d",&op,&x,&y);
			x = (x + last) % 6000 + 1;
			y = (y + last) % 6000 + 1;
			// cout<<x<<' '<<y<<endl;
			if(op == 1)
			{
				scanf("%d",&w);
				vis[x][y] = 1;
				val[x][y] = w;
				tot.push_back(make_pair(x,y));
			}
			else if(op == 2)
			{
				vis[x][y] = 0;
				val[x][y] = 0;
			}
			else if(op == 3)
			{
				scanf("%d%d",&k,&w);
				for(auto &it : f[k])
				{
					for(int i = 0;i<4;i++)
					{
						int xx = x - it.fi*dx[i];
						int yy = y - it.se*dy[i];
						if(check(xx,yy)) s.insert(make_pair(xx,yy));
					}
				}
				for(auto &it : s) val[it.fi][it.se] += w;
			}
			else
			{
				scanf("%d",&k);
				last = 0;
				for(auto it : f[k])
				{
					for(int i = 0;i<4;i++)
					{
						int xx = x - it.fi*dx[i];
						int yy = y - it.se*dy[i];
						if(check(xx,yy)) s.insert(make_pair(xx,yy));
					}
				}
				for(auto &it : s) last += val[it.fi][it.se];
				printf("%lld\n",last);
				//last = ans;
			}
		}
		for(auto it : tot)
		{
			vis[it.fi][it.se] = val[it.fi][it.se] = 0;
		}
	}
	return 0;
}
