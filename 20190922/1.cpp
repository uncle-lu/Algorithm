#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
int line[110];

int main()
{
	read(n);
	for(int i=n;i>=0;i--)
	{
		read(line[i]);
	}

	for(int i=n;i>=0;i--)
	{
		if(line[i]==0)continue;
		if(line[i]>0&&i!=n)printf("+");
		if(abs(line[i])>1 || i==0)printf("%d",line[i]);
		if(line[i]==-1 && i)printf("-");
		if(i>1)printf("x^%d",i);
		if(i==1)printf("x");
	}

	return 0;
}
