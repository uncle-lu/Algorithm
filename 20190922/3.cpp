#include<cstdio>//uncle-lu
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void Euler(int);
bool check(int);
int mul(int, int);

int n,m1,m2,top,m_top,k=0x7f7f7f7f;
int line[10010];
int table[10010],m_table[10010];
bool visit[30010],flag;

void Euler(int x)
{
	for(int i=2;i<=x;++i)
	{
		if(!visit[i])
			table[++top] = i;
		for(int j=1;j<=top&&table[j]*i<=x;++j)
		{
			visit[ table[j] * i ] = true;
			if(i%table[j]==0)break;
		}
	}
	return ;
}

bool check(int x)
{
	for(int i=1;i<=m_top;++i)
		if(x % m_table[i])return false;

	return true;
}

int mul(int x,int divisor)
{
	int cnt=0;

	while(x % divisor == 0)
	{
		cnt++;
		x /= divisor;
	}

	return cnt;
}

int main()
{
	read(n);read(m1);read(m2);
	for(int i=1;i<=n;++i)
		read(line[i]);

	Euler(m1);

	for(int i=1;i<=top && table[i] <= m1;++i)
		if(m1 % table[i] == 0)m_table[++m_top] = table[i];

	
	/*
	 * debug
		for(int i=1;i<=m_top;++i)
			printf("%d ",m_table[i]);
		printf("\n");
	*/

	for(int i=1;i<=n;++i)
	{
		if(!check(line[i]))continue;
		flag = true;
		
		int temp=0;

		for(int j=1;j<=m_top;++j)
		{
			temp = std::max(temp ,(int)ceil( (double) mul(m1, m_table[j]) * m2 / mul(line[i], m_table[j]) ));
		}

		k = std::min(k,temp);
	}

	if(!flag)printf("-1");
	else printf("%d",k);

	return 0;
}
