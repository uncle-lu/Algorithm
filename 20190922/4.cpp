#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n,m,p;
int F[2010];
int sum[2010][2010],r[2010][2010],count[2010];

int main()
{
	read(n);read(m);read(p);

	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;++j)
			read(r[i%n][j]);

	for(int i=0;i<n;++i)
		read(count[i]);

	for(int i=1;i<=m;++i)
		for(int j=0;j<=n;++j)
			sum[i][j] = sum[i-1][(j-1+n)%n] + r[j][i];

	memset(F,-0x7f7f7f7f,sizeof(F));
	F[0] = 0;

	for(int i=1;i<=m;++i)
		for(int j=0;j<=n;++j)
			for(int k=1;i>=k && k<=p;++k)
			{
				int kk = k % n;
				F[i] = std::max(F[i],F[i-k] + sum[i][j] - sum[i-k][(j-kk+n)%n] - count[(j-kk+n)%n]);
			}

	printf("%d",F[m]);

	return 0;
}
