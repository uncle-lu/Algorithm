#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int l, n, m;
int line[50010];

bool check(int x)
{
	int num=0, sit=0;
	for(int i=1;i<=n+1;++i)
	{
		if(line[i] - line[sit] < x)
		{
			num++;
		}
		else
		{
			sit = i;
		}
	}

	if(num <= m)return true;
	else return false;
}

int main()
{
	//freopen("in","r",stdin);
	read(l);read(n);read(m);
	for(int i=1;i<=n;++i)
		read(line[i]);

	line[n+1] = l;

	int left=0, right=l, mid, ans;

	while(left <= right)
	{
		mid = (left + right) >> 1;
		if(check(mid))
		{
			ans = mid;
			left = mid + 1;
		}
		else
		{
			right = mid - 1;
		}
	}

	printf("%d",ans);
	//fclose(stdin);
	return 0;
}
