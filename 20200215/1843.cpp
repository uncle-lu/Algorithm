#include<cstdio>//uncle-lu
#include<cstring>
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
long long int a, b;
long long int sum;
long long int line[500010];
long long int temp[500010];

bool check(long long int x)
{
	for(int i=1;i<=n;++i)
	{
		temp[i] = line[i];
		temp[i] -= (x * a);
	}

	long long int tot = 0;
	for(int i=1;i<=n;++i)
	{
		if(temp[i] > 0)
			tot += temp[i] % b == 0 ? temp[i]/b : temp[i]/b + 1;
	}

	if(tot > x)
		return true;
	else 
		return false;
}

int main()
{
	//freopen("in","r",stdin);
	read(n);read(a);read(b);
	sum = 0;
	for(int i=1; i<=n; ++i)
	{
		read(line[i]);
		sum += line[i];
	}

	long long int left=0, right=sum, mid;
	while(left <= right)
	{
		mid = (left + right) /2 ;
		if( check(mid) )
		{
			left = mid + 1;
		}
		else
		{
			right = mid - 1;
		}
	}

	printf("%lld",left);
	//fclose(stdin);
	return 0;
}
