#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 100010;

struct node{
	int x, y, val;
	friend bool operator<(const node a, const node b)
	{
		return a.val < b.val;
	}
};
node edge[INF];
int Father[310], n, m;

int Find_Father(int x)
{
	return Father[x] == x ? x : Father[x] = Find_Father(Father[x]);
}

int main()
{
	read(n); read(m);
	for(int i=1;i<=n;++i) Father[i] = i;
	for(int i=1; i<=m; ++i)
	{
		read(edge[i].x);read(edge[i].y);read(edge[i].val);
	}

	std::sort(edge+1,edge+1+m);

	int cnt = 0, ans = 0;
	for(int i=1;i<=m;++i)
	{
		int xx = Find_Father(edge[i].x), yy = Find_Father(edge[i].y);
		if(xx != yy)
		{
			Father[yy] = xx;
			ans = edge[i].val;
			cnt++;//
		}
		if(cnt == n-1)break;
	}
	printf("%d %d",n-1, ans);
	return 0;
}
