#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 2010;

struct pos{
	int x,y;
};
struct node{
	int x,y,val;
	friend bool operator<(const node a, const node b)
	{
		return a.val < b.val;
	}
};

pos line[INF];
node edge[INF*INF];
int n,c;
int Father[INF];

int Find_Father(int x)
{
	return Father[x] == x ? x : Father[x] = Find_Father(Father[x]);
}

int main()
{
	read(n);read(c);
	for(int i=1;i<=n;++i)
	{
		read(line[i].x);read(line[i].y);
	}

	int m = 0;
	for(int i=1;i<=n;++i)
	{
		for(int j=i+1;j<=n;++j)
		{
			m++;
			edge[m].x = i;
			edge[m].y = j;
			edge[m].val = (line[i].x - line[j].x)*(line[i].x - line[j].x) + (line[i].y - line[j].y)*(line[i].y - line[j].y);
		}
	}

	for(int i=1; i<=n; ++i) Father[i] = i;
	std::sort(edge+1,edge+1+m);

	int cnt = 0, ans = 0;
	for(int i=1;i<=m;++i)
	{
		if(edge[i].val < c)continue;
		int xx = Find_Father(edge[i].x), yy = Find_Father(edge[i].y);
		if(xx != yy)
		{
			Father[yy] = xx;
			ans += edge[i].val;
			cnt++;
		}
		if(cnt == n-1)break;
	}

	if(cnt!=n-1)
		printf("-1");
	else
		printf("%d",ans);

	return 0;
}
