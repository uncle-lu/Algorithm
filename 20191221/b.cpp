#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n,m;
int line[2010],target[5010];

bool cmp(int a, int b)
{
	return a>b;
}

int main()
{
	read(n); read(m);
	for(int i=1; i<=n; ++i)
		read(line[i]);
	for(int i=1; i<=n; ++i)
		read(target[i]);
	std::sort(line+1,line+1+n,cmp);
	std::sort(target+1,target+1+n,cmp);
	
	for(int i=1;i<=n;++i)
		target[i+n] = target[i];

	int ans = 0x7f7f7f7f;
	for(int i=1;i<=n;++i)
	{
		int x = line[1] <= target[i] ? target[i] - line[1] : target[i] + m - line[1];
		bool flag = true;
		for(int j=1;j<=n;++j)
		{
			if((line[j] + x) % m != target[i+j-1])
			{
				flag = false;
				break;
			}
		}
		if(flag)ans = std::min(ans, x);
	}

	printf("%d",ans);
	return 0;
}
