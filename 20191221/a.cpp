#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

bool check(int x)
{
	for(int i=2;i*i<=x;++i)
		if(x%i == 0)return true;
	return false;
}

int n;

int main()
{
	read(n);
	int a = 2, b = 2 + n;
	while(true)
	{
		if(check(a) && check(b))
			break;
		a++;b++;
	}
	printf("%d %d",b,a);
	return 0;
}
