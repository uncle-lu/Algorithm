#include<cstdio>//uncle-lu
#include<cmath>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int r, s, n;
int line[500010];

int main()
{
	read(r);read(s);read(n);

	int x, y;
	for (int i = 1; i <= n; i++) 
	{
		read(x);read(y);
		if(y <= s) y--;
		line[i] = (r - x + 1) + abs(y - s);
	}
	std::sort(line+1, line+1+n);

	for (int i = 1; i <= n; i++) 
	{
		if(line[i] <= line[i-1])
			line[i] = line[i-1] + 1;
	}

	printf("%d", line[n]);

	return 0;
}
