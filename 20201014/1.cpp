#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>

int n;
char line[20];
char temp[20];

bool is_number(char x)
{
	return x <= '9' && x >= '0';
}

bool is_char(char x)
{
	return (x <= 'z' && x >= 'a') || ( x <= 'Z' && x >= 'A');
}

bool check()
{
	int len_1 = strlen(line), len_2 = strlen(temp);
	int i1 = 0, i2 = 0;

	while(i1 < len_1 && i2 < len_2)
	{
		if(is_number(line[i1]) && is_number(temp[i2]))
		{
			int num1 = 0, num2 = 0;
			while(is_number(line[i1]))
			{
				num1 = num1*10 + line[i1] - '0';
				i1++;
			}
			while(is_number(temp[i2]))
			{
				num2 = num2*10 + temp[i2] - '0';
				i2++;
			}
			if(num1 < num2)
				return true;
			else if(num1 > num2)
				return false;

			continue;
		}
		else if(is_char(line[i1]) && is_char(temp[i2]))
		{
			if(line[i1] < temp[i2])
				return true;
			else if(line[i1] > temp[i2])
				return false;
		}
		else if(is_char(line[i1]) && is_number(temp[i2]))
			return false;
		else
			return true;

		i1++; i2++;
	}

	if(i1 < len_1)
		return false;
	else
		return true;
}

int main()
{
	scanf("%d", &n);
	scanf("%s", line);

	for (int i = 1; i <= n; i++) 
	{
		scanf("%s", temp);

		if(check())
			printf("+\n"); //在后面
		else
			printf("-\n"); //在前面
	}

	return 0;
}
