#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int main()
{
	int T, A, B;
	read(T);
	for(int t=1;t<=T;++t)
	{
		read(A);read(B);
		if((A+B) % 3 != 0)
		{
			printf("NO\n");
			continue;
		}
		int temp = (A+B)/3;
		if(temp > A || temp > B)
		{
			printf("NO\n");
			continue;
		}
		printf("YES\n");
	}
	return 0;
}
