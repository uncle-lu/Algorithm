#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int c,sum;

int main()
{
	int T;
	read(T);
	for(int t=1;t<=T;++t)
	{
		read(c);read(sum);

		if(c>=sum) { printf("%I64d\n",sum); continue; }

		if(sum % c == 0) { printf("%I64d\n", (sum/c)*(sum/c) * c); continue; }

		long long int each = sum/c, each_1 = sum/c + 1;
		long long int s_1 = c - sum%c, s_2 = sum%c;
		printf("%I64d\n",each*each*s_1+each_1*each_1*s_2);

	}
	return 0;
}
