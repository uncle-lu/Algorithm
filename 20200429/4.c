#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int uf_strlen(char *str)
{
	int i = 0;
	while(str[i]!='\0')
		i++;
	return i;
}


char *uf_strcat(char *str1, char *str2)
{
	int len_1 = uf_strlen(str1), len_2 = uf_strlen(str2), i, len_3=0;

	char *str3 = (char*)malloc(sizeof(char)*(len_1+len_2));

	for(i = 0; i < len_1; ++i)
	{
		str3[len_3++] = str1[i];
	}

	for(i = 0; i < len_2; ++i)
	{
		str3[len_3++] = str2[i];
	}

	str3[len_3] = '\0';

	return str3;
}

int main()
{
	char str1[20], str2[20], *str3;

	scanf("%s\n%s", str1, str2);

	str3 = uf_strcat(str1, str2);

	printf("%s\n", str3);

	printf("%d", uf_strlen(str1));

	return 0;
}
