#include<stdio.h>
#include<string.h>

void delchar(char *str1, char x)
{
	if(*str1 == '\0')
		return ;
	if(*str1 == x)
	{
		strcpy(str1, str1+1);
		delchar(str1, x);
	}
	else
	{
		delchar(++str1, x);
	}

	return ;
}

int main()
{
	char str1[100], k;

	gets(str1);
	scanf("%c", &k);

	delchar(str1, k);

	printf("%s", str1);

	return 0;
}
