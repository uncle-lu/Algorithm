#include<stdio.h>

int days[13]={0, 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

void MonthDay(int year, int yearDay, int *pMonth, int *pDay)
{
	if( (year%4!=0) || (year%100==0 && year%400!=0) )
		days[2] = 28;
	else
		days[2] = 29;

	int sum = 0, i;

	for( i=1; i<=12; i++ )
	{
		if(yearDay <= sum+days[i])
		{
			*pMonth = i;
			*pDay = yearDay - sum ;
			break;
		}
		else
			sum = sum + days[i];
	}

	return ;
}

int main()
{
	int year, yearDay, Month, Day;
	scanf("%d %d", &year, &yearDay);

	MonthDay(year, yearDay, &Month, &Day);

	printf("%d-%d-%d", year, Month, Day);

	return 0;
}
