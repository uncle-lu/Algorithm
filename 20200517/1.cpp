#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int val, sit;
	friend bool operator<(const node a, const node b)
	{
		return a.val > b.val;
	}
};
int n_1, n_2;
int line[100010], Tree[100010];
node list[100010];

int lowbit(int x) { return x & (-x); }

void update(int x, int k)
{
	while(x <= n_1 + n_2)
	{
		Tree[x] += k;
		x += lowbit(x);
	}
	return ;
}

int getsum(int x)
{
	int temp = 0;
	while(x > 0)
	{
		temp += Tree[x];
		x -= lowbit(x);
	}
	return temp;
}

int main()
{
	read(n_1);read(n_2);
	for (int i = 1; i <= n_1; i++) 
		read(line[n_1-i+1]);
	for (int i = 1; i <= n_2; i++) 
		read(line[i+n_1]);

	for (int i = 1; i <= n_1+n_2; i++)
	{
		update(i, 1);
		list[i] = (node){line[i], i};
	}

	std::sort(list+1, list+1+n_1+n_2);

	int from = 0, to = n_1;
	long long int ans = 0;
	for (int i = 1; i < n_1+n_2; i++)
	{
		from = to;
		to = list[i].sit;
		if( from < to)
		{
			ans += getsum(to-1) - getsum(from);
			update(to, -1);
		}
		else
		{
			ans += getsum(from) - getsum(to);
			update(to, -1);
		}
	}

	printf("%lld", ans);
	return 0;
}
