#include <cstdio>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 1e5+10;

int n, s;
int line[INF];
long long int sum[INF];

int main()
{
	int T;
	read(T);
	for(int t=1; t<=T; ++t)
	{
		read(n);read(s);
		for(int i=1; i<=n; ++i)
			read(line[i]);

		for(int i=1; i<=n; ++i)
			sum[i] = sum[i-1] + line[i];

		int mx = 0, sit = 0, ans = 0;
		for(int i=1;i<=n;++i)
		{
			if(sum[i] - mx <= s)
				ans = sit;
			if(sum[i] <= s)
				ans = 0;


			if(line[i] > mx)
			{
				sit = i;
				mx = line[i];
			}
		}

		if(sum[n]<=s)
			printf("0\n");
		else
			printf("%d\n",ans);
	}
	return 0;
}
