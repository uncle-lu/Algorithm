#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int main()
{
	int T;
	read(T);
	for(int t=1; t<=T; ++t)
	{
		int a[4];
		read(a[1]);read(a[2]);read(a[3]);
		std::sort(a+1,a+4);
		if(a[1]==a[2]&&a[2]==a[3]&&a[1]==a[3])
			printf("Yes\n");
		else if(a[2]==a[3])
			printf("Yes\n");
		else if(a[3]-a[1]-a[2]<=1)
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}
