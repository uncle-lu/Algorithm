#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int Find_Father(int);

struct node{
	int x,y,val;
	friend bool operator<(const node a, const node b)
	{
		return a.val < b.val;
	}
};
int Father[1000010];
node line[4000010];
int map[1001][1001];
int n, m, cnt;

int Find_Father(int x)
{
	return Father[x] == x ? x : Father[x] = Find_Father(Father[x]);
}

int main()
{
	//freopen("in","r",stdin);

	read(n);read(m);

	int t = 0;
	for(int i=1;i<=n;++i)
		for(int j=1;j<=m;++j)
			map[i][j] = ++t;
	for(int i=1;i<=t;++i)Father[i] = i;

	int x1, y1, x2, y2;
	while(~scanf("%d %d %d %d", &x1, &y1, &x2, &y2))
	{
		int a1 = map[x1][y1], a2 = map[x2][y2];
		a1 = Find_Father(a1);
		a2 = Find_Father(a2);
		Father[a1] = a2;
	}

	for(int i=1;i<=n;++i)
		for(int j=1;j<=m;++j)
		{
			if(i+1<=n)
			{
				line[++cnt].x = map[i+1][j];
				line[cnt].y = map[i][j];
				line[cnt].val = 1;
			}
			if(j+1<=m)
			{
				line[++cnt].x = map[i][j+1];
				line[cnt].y = map[i][j];
				line[cnt].val = 2;
			}
		}

	std::sort(line+1,line+1+cnt);

	int ans = 0;
	for(int i=1;i<=cnt;++i)
	{
		int xx = Find_Father(line[i].x), yy = Find_Father(line[i].y);
		if(xx!=yy)
		{
			Father[yy]=xx;
			ans += line[i].val;
		}
	}

	printf("%d",ans);

	//fclose(stdin);
	return 0;
}
