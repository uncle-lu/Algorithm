#include <stdio.h>
int main()
{
  long long int n,i;//n未知数 
  scanf("%lld",&n);//输入 
  
  while( n ! = 1 )//while循环 
  {
    if(n%2==0)//如果n是双数 
    {
      printf("%lld/2=%lld\n",n,n/2);//输出n/2=(n/2)
      n = n/2;//重新赋值 
      
    }
    else//否则（n是单数） 
    {
      printf("%lld*3+1=%lld\n",n,n*3+1);//输出n*3+1=(n*3+1)
      n = n*3+1;//重新赋值 
    }
  }
  printf("End");
  return 0;
}