#include<cstdio>//uncle-lu
#include<cstring>
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void add_edge(int, int, int);
void spfa(void);

struct qu_node{
	int x, sit;
};
struct node{
	int v, next, val;
};
node edge[20010];
int head[1010], cnt;
int d[1010][1010];
bool visit[1010][1010];
int n, m, ans, k;

void add_edge(int x, int y, int val)
{
	edge[++cnt].v = y;
	edge[cnt].next = head[x];
	edge[cnt].val = val;
	head[x] = cnt;
	return ;
}

void spfa()
{
	std::queue<qu_node>qu;

	memset(d,0x7f7f7f7f,sizeof(d));
	d[1][0] = 0;
	visit[1][0] = true;
	qu.push((qu_node){1, 0});
	while(!qu.empty())
	{
		qu_node now(qu.front());
		qu.pop();
		if(now.x == n)
			ans = std::min(ans, d[now.x][now.sit]);
		visit[now.x][now.sit] = false;
		for(int i=head[now.x];~i;i=edge[i].next)
		{
			if(d[edge[i].v][now.sit] > std::max(d[now.x][now.sit], edge[i].val))
			{
				d[edge[i].v][now.sit] = std::max(d[now.x][now.sit], edge[i].val);
				if(!visit[edge[i].v][now.sit])
				{
					visit[edge[i].v][now.sit] = true;
					qu.push((qu_node){edge[i].v, now.sit});
				}
			}
			if(now.sit + 1 <= k && d[edge[i].v][now.sit+1] > d[now.x][now.sit])
			{
				d[edge[i].v][now.sit+1] = d[now.x][now.sit];
				if(!visit[edge[i].v][now.sit+1])
				{
					visit[edge[i].v][now.sit+1] = true;
					qu.push((qu_node){edge[i].v, now.sit+1});
				}
			}
		}

	}

	if(ans == 0x7f7f7f7f)
		ans = -1;

	return ;
}

int main()
{
	int x,y,val;

	memset(head, -1, sizeof(head));
	read(n);read(m);read(k);
	for(int i=1;i<=m;++i)
	{
		read(x); read(y); read(val);
		add_edge(x, y, val);
		add_edge(y, x, val);
	}

	ans = 0x7f7f7f7f;

	spfa();

	printf("%d",ans);

	return 0;
}
