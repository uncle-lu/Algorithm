#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
#include<queue>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void add_edge(int, int, int);
void spfa(void);

struct qu_node{
	int x, sit;
};
struct node{
	int v, next, val;
};
node edge[100010];
int head[10010],cnt;
int d[10010][20];
bool visit[10010][20];
int n, m, k, ans, start, last;

void add_edge(int x, int y, int val)
{
	edge[++cnt].v = y;
	edge[cnt].next = head[x];
	edge[cnt].val = val;
	head[x] = cnt;
	return ;
}

void spfa()
{
	memset(d,0x7f7f7f7f,sizeof(d));
	d[start][0] = 0;
	visit[start][0] = true;
	ans = 0x7f7f7f7f;
	std::queue<qu_node>qu;
	qu.push((qu_node){start,0});
	while(!qu.empty())
	{
		qu_node now = qu.front(); qu.pop();
		int x = now.x, sit = now.sit;
		visit[x][sit] = false;
		if(x == last)
			ans = std::min(ans, d[x][sit]);
		for(int i=head[x];~i;i=edge[i].next)
		{
			if(d[edge[i].v][sit] > d[x][sit] + edge[i].val)
			{
				d[edge[i].v][sit] = d[x][sit] + edge[i].val;
				if(!visit[edge[i].v][sit])
				{
					visit[edge[i].v][sit] = true;
					qu.push((qu_node){edge[i].v, sit});
				}
			}
			if(sit+1 <= k && d[edge[i].v][sit+1] > d[x][sit])
			{
				d[edge[i].v][sit+1] = d[x][sit];
				if(!visit[edge[i].v][sit+1])
				{
					visit[edge[i].v][sit+1] = true;
					qu.push((qu_node){edge[i].v, sit+1});
				}
			}
		}
	}

	return ;
}

int main()
{
	int x, y, val;
	memset(head, -1, sizeof(head));
	read(n); read(m); read(k);
	read(start); read(last);
	for(int i=1;i<=m;++i)
	{
		read(x); read(y); read(val);
		add_edge(x, y, val);
		add_edge(y, x, val);
	}

	spfa();

	printf("%d",ans);

	return 0;
}
