#include<cstdio>//uncle-lu
#include<cstring>
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void add_edge(int, int, int);
void spfa(int);

struct node{
	int v, next, val;
};
node edge[3010];
int head[810],cnt;
int tot_d[810], line[810], d[810];
int p,n,m;
bool visit[810];

void add_edge(int x, int y, int val)
{
	edge[++cnt].v = y;
	edge[cnt].next = head[x];
	edge[cnt].val = val;
	head[x] = cnt;
	return ;
}

void spfa(int start)
{
	std::queue<int>qu;
	qu.push(start);
	visit[start] = true;
	while(!qu.empty())
	{
		int now = qu.front();
		qu.pop();
		visit[now] = false;

		for(int i=head[now];~i;i=edge[i].next)
		{
			if(d[edge[i].v] > d[now] + edge[i].val)
			{
				d[edge[i].v] = d[now] + edge[i].val;
				if(!visit[edge[i].v])
				{
					visit[edge[i].v] = true;
					qu.push(edge[i].v);
				}
			}
		}
	}

	return ;
}

int main()
{
	int x, y, val;
	memset(head,-1,sizeof(head));

	read(p); read(n); read(m);
	for(int i=1;i<=p;++i)
		read(line[i]);
	for(int i=1;i<=m;++i)
	{
		read(x); read(y); read(val);

		add_edge(x,y,val);
		add_edge(y,x,val);
	}

	for(int i=1;i<=p;++i)
	{
		memset(d,0x7f7f7f7f,sizeof(d));
		memset(visit,false,sizeof(visit));
		d[line[i]] = 0;

		spfa(line[i]);

		for(int j=1;j<=n;++j)
			tot_d[j] += d[j];
	}

	int ans = 0x7f7f7f7f;
	for(int i=1;i<=n;++i)
		ans = std::min(ans, tot_d[i]);

	printf("%d",ans);

	return 0;
}
