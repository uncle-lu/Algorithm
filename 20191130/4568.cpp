#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
#include<queue>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void add_edge(int, int, int);
void dijkstra(void);

struct Heap_node{
	int x, sit, d;
	friend bool operator<(const Heap_node a,const Heap_node b)
	{
		return a.d>b.d;
	}
};
struct node{
	int v, next, val;
};
node edge[100010];
int head[10010],cnt;
int d[10010][15];
int visit[10010][15];
int ans, start, last, k, n, m;

void add_edge(int x, int y, int val)
{
	edge[++cnt].v = y;
	edge[cnt].next = head[x];
	edge[cnt].val = val;
	head[x] = cnt;
	return ;
}

void dijkstra()
{
	memset(d,0x7f7f7f7f,sizeof(d));
	std::priority_queue<Heap_node>qu;
	d[start][0] = 0;
	qu.push((Heap_node){start, 0, 0});

	while(!qu.empty())
	{
		Heap_node now(qu.top());
		qu.pop();
		if(now.x == last)
			ans = std::min(ans, d[now.x][now.sit]);
		if(visit[now.x][now.sit])continue;
		int x = now.x, sit = now.sit;
		
		for(int i=head[x];~i;i=edge[i].next)
		{
			if(d[edge[i].v][sit] > d[x][sit] + edge[i].val)
			{
				d[edge[i].v][sit] = d[x][sit] + edge[i].val;
				qu.push((Heap_node){ edge[i].v, sit, d[edge[i].v][sit] });
			}
			if(sit+1 <= k && d[edge[i].v][sit+1] > d[x][sit])
			{
				d[edge[i].v][sit+1] = d[x][sit];
				qu.push((Heap_node){ edge[i].v, sit+1, d[edge[i].v][sit+1] });
			}
		}
	}

	return ;
}

int main()
{
	int x, y, val;
	memset(head,-1,sizeof(head));

	read(n); read(m); read(k);
	read(start); read(last);
	for(int i=1;i<=m;++i)
	{
		read(x);read(y);read(val);
		add_edge(x, y, val);
		add_edge(y, x, val);
	}

	ans = 0x7f7f7f7f;
	dijkstra();

	printf("%d",ans);
	return 0;
}
