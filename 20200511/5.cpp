#include<cstdio>
#include<queue>
#include<vector>

std::priority_queue<int, std::vector<int>, std::greater<int> > que;
int n, x, ans;

int main()
{
	scanf("%d", &n);
	for(int i=1; i<=n; ++i)
	{
		scanf("%d", &x);
		que.push(x);
	}

	while(que.size()>=2)
	{
		int a = que.top();
		que.pop();
		int b = que.top();
		que.pop();
		ans += (a+b);
		que.push(a+b);
	}

	printf("%d", ans);
	return 0;
}
