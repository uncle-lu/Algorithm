#include<iostream>//uncle-lu
#include<string>
#include<cstdlib>

bool flag[300];
int sta[300];
std::string str;
int top_sta, top_f;

void pop()
{
	switch(sta[top_sta])
	{
		case 1:
			if(top_f < 2)
			{
				std::cout << "error";
				exit(0);
			}
			flag[top_f - 1] = flag[top_f - 1] || flag[top_f];
			top_f--;
			break;
		case 2:
			if(top_f < 2)
			{
				std::cout << "error";
				exit(0);
			}
			flag[top_f - 1] = flag[top_f - 1] && flag[top_f];
			top_f--;
			break;
		case 3:
			if(!top_f)
			{
				std::cout << "error";
				exit(0);
			}
			flag[top_f] = !flag[top_f];
			break;
	}
	top_sta--;
	return ;
}

int main()
{
	while(std::cin >> str)
	{
		if(str == "and")
		{
			while(top_sta && sta[top_sta] > 2)
				pop();
			sta[++top_sta] = 2;
		}
		else if(str == "or")
		{
			while(top_sta)
				pop();
			sta[++top_sta] = 1;
		}
		else if(str == "not")
			sta[++top_sta] = 3;
		else if(str == "true")
			flag[++top_f] = true;
		else if(str == "false")
			flag[++top_f] = false;
	}

	while(top_sta)
		pop();

	if(top_f == 1)std::cout << (flag[1] ? "true" : "false");
	else std::cout << "error";

	return 0;
}

/*
 * 使用栈模拟中缀表达式
 *
 * 一个符号栈，一个值的栈
 *
 * 如果读入符号 ，我们就要将符号栈中比该读入符号小的符号全部计算出来。
 *
 * 如果计算过程中出现问题，就停止程序
 *
 * 如果最后计算完成后 值栈中还有元素没有计算掉，说明少符号了，停止程序。
 *
 */
