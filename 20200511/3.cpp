#include<cstdio>//uncle-lu
#include<algorithm>

int n, m;
int line[500010], sum[500010];
int que[500010], head, last;

int main()
{
	scanf("%d %d", &n, &m);

	for(int i=1; i<=n; ++i)
		scanf("%d", &line[i]);

	for(int i=1; i<=n; ++i)
		sum[i] = sum[i-1] + line[i];

	int ans = -1e9;
	for(int i=1; i<=n; ++i)
	{
		while(head >= last && que[last] <= i-m)last++;
		ans = std::max(ans, sum[i] - sum[que[last]]);
		while(head >= last && sum[i] <= sum[ que[head] ])head--;
		que[++head] = i;
	}

	printf("%d\n", ans);
	return 0;
}
