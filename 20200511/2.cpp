#include<cstdio>//uncle-lu
#include<algorithm>

int n, line[2000010], sum[2000010], cnt;
int que[2000010], head, last, sit[2000010];

int main()
{
	scanf("%d", &n);
	for(int i=1; i<=n; ++i)
	{
		scanf("%d", &line[i]);
		line[i+n] = line[i];
	}

	for(int i=1; i<=n*2; ++i)
		sum[i] = sum[i-1] + line[i];

	for(int i=1; i<=n*2; ++i)
	{
		while(head >= last && que[head] > sum[i])
			head--;
		head++;
		que[head] = sum[i]; sit[head] = i;
		while(head >= last && sit[last] <= i-n)
			last++;

		if(i > n && que[last] - sum[i-n] >= 0)cnt++;
	}

	printf("%d", cnt);

	return 0;
}
