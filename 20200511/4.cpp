#include<cstdio>//uncle-lu
#include<queue>

const int N = 100010;
int line_1[N], line_2[N];
int n;

struct node{
	int sit_1, sit_2, val;
	friend bool operator<(const node a, const node b)
	{
		return a.val > b.val;
	}
};

std::priority_queue<node>qu;

int main()
{
	scanf("%d", &n);

	for(int i=1; i<=n; ++i)
		scanf("%d", &line_1[i]);

	for(int i=1; i<=n; ++i)
		scanf("%d", &line_2[i]);

	for(int i=1; i<=n; ++i)
		qu.push((node){1, i, line_1[1] + line_2[i]});

	node now;
	for(int i=1; i<=n; ++i)
	{
		now = qu.top();
		qu.pop();
		printf("%d ",now.val);

		now.sit_1++;
		now.val = line_1[now.sit_1] + line_2[now.sit_2];
		qu.push(now);
	}

	return 0;
}
