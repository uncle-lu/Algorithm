#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int F[110][110];
int n,m;

int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);

	read(n);read(m);

	F[0][0] = 1;

	for(int i=1;i<=m;++i)
		for(int j=0;j<=n;++j)
		{
			F[j][i] = F[ (j-1+n) % n ][i-1] + F[ (j+1) % n ][i-1];
		}

	printf("%d",F[0][m]);

	fclose(stdin);
	fclose(stdout);
	return 0;
}
