#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int F[300010];
int m,s,t;

int main()
{
	freopen("escape.in","r",stdin);
	freopen("escape.out","w",stdout);

	read(m);read(s);read(t);

	for(int i=1;i<=t;++i)
	{
		if(m>=10){ F[i] = F[i-1] + 60; m-=10; }
		else { F[i] = F[i-1] ; m += 4; }
	}

	for(int i=1;i<=t;++i)
	{
		F[i] = std::max(F[i-1] + 17,F[i]);
		if(F[i] >= s)
		{
			printf("Yes\n%d",i);
			return 0;
		}
	}

	printf("No\n%d",F[t]);

	fclose(stdin);
	fclose(stdout);
	return 0;
}
