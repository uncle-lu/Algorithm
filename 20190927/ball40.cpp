#include<cstdio>//uncle-lu
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int sit,cnt;
	node(int s, int c){ sit = s; cnt = c; }
};

int n,m;
std::queue<node>qu;
int ans;

int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);

	read(n);read(m);

	node now(0,0);
	qu.push(now);

	while(!qu.empty())
	{
		now = qu.front();
		qu.pop();
		if(now.sit == 0 && now.cnt == m)
			ans++;

		if(now.cnt >= m)continue;

		if(now.sit - 0 > m && n - now.sit > m)continue;

		qu.push( (node){ (now.sit + 1) % n, now.cnt+1 } );
		qu.push( (node){ (now.sit - 1 + n) % n, now.cnt+1 } );
	}

	printf("%d",ans);

	fclose(stdin);
	fclose(stdout);

	return 0;
}
