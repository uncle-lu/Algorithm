#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int val,sit;
	node(int v = 0, int s = 0){ val = v; sit = s; }
	friend bool operator<(const node a,const node b) { return a.val>b.val; }
};

int N,M,K,L,D;
int h[1010],l[1010];
node hi[1010];
int ans[1010];

int main()
{
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);

	read(N);read(M);read(K);read(L);read(D);

	int a,b,c,d;
	for(int i=1;i<=D;++i)
	{
		read(a);read(b);read(c);read(d);
		if(a==c)
		{
			h[std::min(b,d)]++;
		}
		else
		{
			l[std::min(a,c)]++;
		}
	}

	int temp = 0;
	for(int i=1;i<=N;++i)
		if(l[i])
		{
			temp++;
			hi[temp].val = l[i];
			hi[temp].sit = i;
		}

	std::sort(hi+1,hi+1+temp);
	for(int i=1;i<=K;++i)
		ans[i] = hi[i].sit;
	std::sort(ans+1,ans+1+K);
	for(int i=1;i<=K;++i)
		printf("%d ",ans[i]);

	printf("\n");

	temp = 0;
	for(int i=1;i<=M;++i)
		if(h[i])
		{
			temp++;
			hi[temp].val = h[i];
			hi[temp].sit = i;
		}

	std::sort(hi+1,hi+1+temp);
	for(int i=1;i<=L;++i)
		ans[i] = hi[i].sit;
	std::sort(ans+1,ans+1+L);
	for(int i=1;i<=L;++i)
		printf("%d ",ans[i]);

	fclose(stdin);
	fclose(stdout);

	return 0;
}
