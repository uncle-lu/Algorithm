#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>

char line[20];

int main()
{
	freopen("isbn.in","r",stdin);
	freopen("isbn.out","w",stdout);
	scanf("%s",line+1);
	int tot=0,cnt=0;
	for(int i=1;i<=11;++i)
	{
		if(line[i]=='-')continue;
		tot++;
		cnt += ( tot * (line[i]-'0') );
	}

	if(line[13]=='X')line[13] = 10;
	else line[13] -= '0';

	if(line[13] == cnt % 11)
		printf("Right");
	else
	{
		if(cnt%11 == 10)
			line[13] = 'X';
		else
			line[13] = '0' + (cnt % 11);

		printf("%s",line+1);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
