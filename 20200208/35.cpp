#include <cstdio>//uncle-lu

int main()
{
	int n;
	double e = 1, sum = 1;
	scanf("%d",&n);

	for(int i=1; i<=n; ++i)
	{
		sum = sum * i;
		e = e + (double)1/sum;
	}

	printf("%.10lf",e);

	return 0;
}
