#include<cstdio>//uncle-lu

int main()
{
	int n, cnt=0;
	scanf("%d",&n);

	for(int i=1;i<=n;++i)
	{
		int temp = i;
		while(temp>0)
		{
			if(temp%10==1)cnt++;
			temp=temp/10;
		}
	}

	printf("%d", cnt);
	return 0;
}
