#include<cstdio>//uncle-lu
#include<cmath>

int main()
{
	int m, n, x;
	scanf("%d%d%d",&m,&n,&x);

	while(x!=0)
	{
		if(m > x * n)
			break;
		x -= ceil((double)m/n);

		n += m/n;
	}

	printf("%d",n);

	return 0;
}
