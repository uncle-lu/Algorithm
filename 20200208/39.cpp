#include <cstdio>//uncle-lu

int main()
{
	int n, sum=0;
	scanf("%d",&n);

	for(int i=1;i<=n;++i)
	{
		if(i%7!=0)
		{
			int temp = i;
			bool flag = false;
			while(temp)
			{
				if(temp%10 == 7)
				{
					flag = true;
					break;
				}
				temp = temp / 10;
			}
			if(!flag)sum += i*i;
		}
	}

	printf("%d",sum);
	return 0;
}
