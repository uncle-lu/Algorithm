#include <cstdio>//uncle-lu
#include <cmath>
#include <cstring>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 1010;

struct node{
	int x, y;
	double val;
	friend bool operator<(const node a, const node b)
	{
		return a.val < b.val;
	}
};
node edge[INF*INF];
int cnt;
int line[INF];
int x[INF],y[INF];
int Father[INF];
int n,m;

int Find_Father(int a)
{
	return Father[a] == a ? a : Father[a] = Find_Father(Father[a]);
}

int main()
{
	read(n);
	for(int i=1;i<=n;++i)
		read(line[i]);
	read(m);
	for(int i=1;i<=m;++i)
	{
		read(x[i]);read(y[i]);
	}
	for(int i=1;i<=m;++i)
		Father[i] = i;

	for(int i=1;i<=m;++i)
		for(int j=i+1;j<=m;++j)
			edge[++cnt]=(node){ i,j,(double)sqrt((double)(x[i]-x[j])*(x[i]-x[j]) + (double)(y[i]-y[j])*(y[i]-y[j])) };

	std::sort(edge+1, edge+1+cnt);

	double mx=0;
	int k = 0;
	for(int i=1;i<=cnt;++i)
	{
		int xx = Find_Father(edge[i].x), yy = Find_Father(edge[i].y);
		if(xx != yy)
		{
			Father[yy] = xx;
			mx = edge[i].val;
			k++;
		}
		if(k==m-1)break;
	}

	int ans=0;
	for(int i=1;i<=n;++i)
		if(line[i]>=mx)ans++;

	printf("%d",ans);

	return 0;
}
