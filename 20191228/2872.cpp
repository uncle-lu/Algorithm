#include <cstdio>//uncle-lu
#include <cmath>
#include <cstring>
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int INF = 1010;

int Find_Father(int);

struct node{
	int x, y;
	double val;
	friend bool operator<(const node a, const node b)
	{
		return a.val < b.val;
	}
};
node edge[INF*INF];
int x[INF], y[INF];
int Father[INF];
int cnt, n, m;

int Find_Father(int xx)
{
	return Father[xx] == xx ? xx : Find_Father(Father[xx]);
}


int main()
{
	read(n);read(m);
	for(int i=1;i<=n;++i)Father[i] = i;

	for(int i=1;i<=n;++i)
	{
		read(x[i]);read(y[i]);
	}

	int a, b;
	for(int i=1;i<=m;++i)
	{
		read(a); read(b);
		int xx = Find_Father(a), yy = Find_Father(b);
		Father[yy] = xx;
	}

	for(int i=1;i<=n;++i)
		for(int j=i+1;j<=n;++j)
			edge[++cnt]=(node){ i, j , (double)sqrt((double)(x[i]-x[j])*(x[i]-x[j]) + (double)(y[i]-y[j])*(y[i]-y[j]))};

	std::sort(edge+1, edge+1+cnt);

	double ans = 0.0;
	for(int i=1;i<=cnt;++i)
	{
		int xx = Find_Father(edge[i].x), yy = Find_Father(edge[i].y);
		if(xx!=yy)
		{
			Father[yy] = xx;
			ans += edge[i].val;
		}
	}

	printf("%.2lf",ans);

	return 0;
}
