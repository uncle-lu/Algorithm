#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
int line[5010], F[5010], visit[50101];

int main(void)
{
	while(~scanf("%d",&n))
	{
		for(int i=1;i<=n;++i)
			read(line[i]);

		for(int i=1;i<=n;++i)
		{
			F[i] = 1;
			for(int j=1;j<i;++j)
				if(line[j]<line[i])
					F[i] = std::max(F[i],F[j]+1);
		}

		for(int i=1;i<=n;++i)
		{
			memset(visit,0x7f7f7f7f,sizeof(visit));
			visit[0] = 0;
			int ans=0;
			for(int j=1;j<i;++j)
			{
				ans ^= (F[j] * F[j]);
				visit[F[j]] = std::min(visit[F[j]], line[j]);
			}

			for(int j=i+1;j<=n;++j)
			{
				if(visit[F[j]-1] < line[j])
				{
					ans ^= (F[j] * F[j]);
					visit[F[j]] = std::min(line[j], visit[F[j]]);
				}
				else
				{
					ans ^= (F[j]-1) * (F[j]-1);
					visit[F[j]-1] = std::min(line[j], visit[F[j]-1]);
				}
			}
			printf("%d ",ans);
		}
		printf("\n");
	}
	return 0;
}
