#include <cstdio>//uncle-lu
#include <algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int main()
{
	int r, c;
	read(r); read(c);
	if( r == 1 && c == 1 )
	{
		printf("0");
		return 0;
	}
	if( c == 1 )
	{
		for(int i=1;i<=r;++i)
			printf("%d\n",i+1);
		return 0;
	}
	for(int i=1;i<=r;++i)
	{
		for(int j=1;j<=c;++j)
		{
			printf("%lld ",(long long int)i*(j+r));
		}
		printf("\n");
	}
	return 0;
}
