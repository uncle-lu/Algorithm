#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;bool f=false;char ch=getchar();
	while(ch<'0'||ch>'9'){ f|=(ch=='-'); ch = getchar(); }
	while(ch<='9'&&ch>='0') { x = (x<<1) + (x<<3) + (ch^48); ch = getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
int mi,mx;
int line[100010];

int main()
{
	read(n);
	for(int i=1;i<=n;++i)
		read(line[i]);

	mi = line[1]; mx = line[1];

	for(int i=2;i<=n;++i)
	{
		mi = std::min(line[i],mi);
		mx = std::max(line[i],mx);
	}

	printf("%d ",mx);

	if(n%2)
	{
		printf("%d ",line[n/2+1]);
	}
	else
	{
		int k = line[n/2 + 1] + line[n/2];
		if(k%2)
		{
			printf("%.1lf ",(double)k/2);
		}
		else
		{
			printf("%d ",k/2);
		}
	}

	printf("%d",mi);

	return 0;
}
