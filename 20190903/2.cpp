#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x = 0 ; bool f = false; char ch = getchar();
	while(ch<'0'||ch>'9'){ f |= (ch=='-'); ch = getchar(); }
	while(ch<='9'&&ch>='0') { x = (x<<1) + (x<<3) + (ch^48); ch = getchar(); }
	x = f ? -x : x;
	return ;
}

void solution(void);

char line[10];
int tong[10],top;

void solution()
{
	memset(tong,0,sizeof(tong));
	top=0;
	gets(line+1);
	for(int i=1;i<=7;++i)
	{
		if(line[i]<='9'&&line[i]>='0')
		{
			top++;
			tong[top] = line[i] - '0';
		}
		else if(line[i]=='-')
		{
			top++;
			tong[top] = - (line[i+1] - '0');
			i++;
		}
		else if(line[i]=='x')
		{
			tong[top] = tong[top] * (line[i+1] - '0');
			i++;
		}
		else if(line[i]=='/')
		{
			tong[top] = tong[top] / (line[i+1] - '0');
			i++;
		}
	}
	while(top)
	{
		tong[top-1] += tong[top];
		top--;
	}
	if(tong[0]==24)printf("Yes\n");
	else printf("No\n");
	return ;
}

int main()
{
	//freopen("in","r",stdin);
	int T;
	read(T);
	for(int i=1;i<=T;++i)
	{
		solution();
	}
	//fclose(stdin);
	return 0;
}
