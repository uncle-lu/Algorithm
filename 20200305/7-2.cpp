/*
 * Author: uncle-lu 21190603	
 * Mail: uncle-lu@qq.com
 * Blog: uncle-lu.org
 * File name: 7-2.cpp
 * Description: C++ course assignments.
 */
#include<iostream>
#include<cstring>

class MyString{
	private:
		char* line;
	public:
		MyString():line(NULL){}
		MyString(const char *temp)
		{
			line = new char[strlen(temp)+1];
			if(line)
				strcpy(line, temp);
		}
		MyString(const MyString& temp)
		{
			line = new char[strlen(temp.line)+1];
			if(line)
				strcpy(line, temp.line);
		}
		~MyString()
		{
			delete[] line;
		}

		char& operator [](const int sit)
		{
			return line[sit];
		}

		MyString& operator =(const MyString& temp)
		{
			delete []line;
			line = new char[strlen(temp.line) + 1];
			if(line)
				strcpy(line, temp.line);
			return *this;
		}

		MyString& operator =(const char* temp)
		{
			delete []line;
			line = new char[strlen(temp) + 1];
			if(line)
				strcpy(line, temp);
			return *this;
		}

		MyString operator +(const MyString& temp)
		{
			char *nline = new char[strlen(line) + strlen(temp.line) + 1];
			for(int i=0; i<(int)strlen(line); i++)
				nline[i] = line[i];

			for(int i=0; i<(int)strlen(temp.line); i++)
				nline[i+strlen(line)] = temp.line[i];

			nline[strlen(line) + strlen(temp.line)] = '\0';

			return MyString(nline);
		}

		MyString operator +(const char* temp)
		{
			char *nline = new char[strlen(line) + strlen(temp) + 1];
			for(int i=0; i<(int)strlen(line); i++)
				nline[i] = line[i];

			for(int i=0; i<(int)strlen(temp); i++)
				nline[i+strlen(line)] = temp[i];

			nline[strlen(line) + strlen(temp)] = '\0';

			return MyString(nline);
		}

		bool empty();
		void clear();	
		int find(const char);
		int lenth();
		void print();
};

bool MyString::empty()
{
	if(line == NULL)
		return true;
	return false;
}

void MyString::clear()
{
	if(line == NULL)
		return ;
	delete[] line;
	line = NULL;
	return ;
}

int MyString::find(const char temp)
{
	for(size_t i=0; i < strlen(line); ++i)
	{
		if(line[i] == temp)
			return i;
	}
	return -1;
}

int MyString::lenth()
{
	return strlen(line);
}

void MyString::print()
{
	std::cout << line << std::endl;
	return ;
}

int main()
{
	MyString l_1("HelloWorld");
	MyString l_2("OldWorld");

	l_1.print();
	std::cout << l_1[4] << std::endl;
	std::cout << l_1.lenth() << std::endl;

	MyString l_3;
	l_3 = l_1 + l_2;
	l_3.print();

	MyString l_4;
	l_4 = l_1 + "---" + l_2;
	l_4.print();

	MyString l_5;
	l_5 = "QwQQAQQsQ";
	l_5.print();

	if(!l_5.empty())
		l_5.clear();

	if(l_5.empty())
		std::cout << "Empty" << std::endl;

	return 0;
}
