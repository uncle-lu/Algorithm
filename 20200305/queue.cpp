/*
 * Author: uncle-lu 21190603
 * Mail: uncle-lu@qq.com
 * Blog: uncle-lu.org
 * File name: queue.cpp
 * Description: C++ course assignments.
 */
#include <cstdio>
#include <cstring>
#include <cassert>
#include <iostream>

template<class QDataType1>
struct QNode1
{
	struct QNode1* _pNext;
	QDataType1 _data;
};

template<class QDataType1>
class QUEUE
{
	QNode1<QDataType1>* _front;
	QNode1<QDataType1>* _back;
	public:
	QUEUE():_front(NULL),_back(NULL){}
	~QUEUE()
	{
		QNode1<QDataType1>* now = _front;
		QNode1<QDataType1>* next = _front;
		while(now!=_back)
		{
			next = now->_pNext;
			delete now;
			now = next;
		}
		delete now;
	}
	/*
	void QueuePush(QDataType1 data);
	void QueuePop();
	QDataType1 QueueFront();
	QDataType1 QueueBack();
	int QueueSize();
	int QueueEmpty();
	void QueueDestroy();
	*/
	void QueuePush(QDataType1 data)
	{
		QNode1<QDataType1>* newnode = new QNode1<QDataType1>;

		assert(newnode);

		newnode->_data = data;
		newnode->_pNext = NULL;

		if (_front == NULL )
		{
			_front = _back = newnode;
		}
		else
		{
			_back->_pNext = newnode;
			_back = newnode;
		}
	}

	void QueuePop()
	{
		if (_front == NULL)
		{
			return;
		}
		QNode1<QDataType1>* newhead = _front;

		if (_front->_pNext == NULL)
		{
			_front = _back = NULL;
		}
		else
		{
			_front = newhead->_pNext;

		}
		delete newhead;
	}

	QDataType1 QueueFront()
	{
		if (_front == NULL)
		{
			return NULL;
		}
		return _front->_data;
	}

	QDataType1 QueueBack()
	{
		if (_front == NULL)
		{
			return NULL;
		}
		return _back->_data;
	}

	int QueueSize()
	{
		QNode1<QDataType1>* pCur = _front;
		int count = 0;

		while (pCur != NULL)
		{
			pCur = pCur->_pNext;
			count++;
		}
		return count;
	}

	int QueueEmpty()
	{
		if (_front == NULL)
		{
			return 1;
		}
		return -1;
	}

	void QueueDestroy()
	{
		if (_front == NULL)
		{
			return;
		}
		QNode1<QDataType1>* pCur = _front;

		while (pCur != NULL)
		{
			_front= pCur->_pNext;
			delete pCur;
			pCur = _front;
		}
		_front = _back = NULL;
	}
};

void QueueTEST()
{
	QUEUE<double>s;
	s.QueuePush(1.1);
	s.QueuePush(11.11);
	s.QueuePush(111.111);
	s.QueuePush(1111.111);
	printf("size=%d\n",s.QueueSize());
	//出队列
	s.QueuePop();
	s.QueuePop();
	s.QueuePop();

	//查看当前元素个数
	printf("size=%d\n",s.QueueSize());
}

int main()
{
	QueueTEST();
	return 0;
}

