#include<cstdio>//uncle-lu

int n, k, mod;

long long int f(int x)
{
	long long int res = 1;
	for (int i = 1; i <= x; i++) 
	{
		res*=i;
		res%=mod;
	}
	return res;
}

int main()
{
	int T;
	scanf("%d", &T);
	for(int t = 1; t<=T; ++t)
	{
		scanf("%d %d %d", &n, &k, &mod);
		long long int ans = 0;
		if(k < n)
		{
			ans = f(k) * ( n-k + k * (n - k) % mod + (n-k-1) * (n-k-1) % mod) % mod;
		}
		else
		{
			ans = f(n) % mod;
		}
		printf("Case #%d: %lld\n", t, ans);
	}
	return 0;
}
