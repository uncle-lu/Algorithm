#include <cstdio>//uncle-lu
#include <vector>

const int N = 1e6+10;

int line[N], nex[N], prime[N], tot, n;
bool visit[N];
std::vector<std::vector<int> >number;

void init()
{
	for (int i = 2; i <= N; i++) 
	{
		if(!visit[i])
			prime[++tot] = i;
		for (int j = 1; j <= tot && (long long int)i * prime[j] < N ; j++) 
		{
			visit[i*prime[j]] = true;
			if(i % prime[j] == 0) break;
		}
	}
	number.clear();
	number.resize(N);
	for (int i = 1; i <= tot; i++) 
		for (int j = prime[i]; j < N; j+=prime[i]) 
			number[j].push_back(prime[i]);

	for (int i = 1; i < N; i++) 
		nex[i] = n + 1;
	return ;
}

int main()
{
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) 
		scanf("%d", &line[i]);
	init();

	long long int ans = 0, base = 0;
	for (int i = n; i >= 1; --i) 
	{
		for(auto it:number[line[i]])
		{
			base += (nex[it] - i);
			nex[it] = i;
		}
		ans += base;
	}

	printf("%lld", ans);
	return 0;
}
