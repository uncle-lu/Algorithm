#include<cstdio>//uncle-lu
#include<cstring>
#include<vector>
#include<set>

const int N = 6000 + 10;

struct node{
	int x,y;
	friend bool operator<(const node a, const node b)
	{
		return (a.x == b.x && a.y < b.y) || (a.x < b.x);
	}
};
bool visit[N][N];
int val[N][N];
int n, m;

int x_turn[4]={1,1,-1,-1};
int y_turn[4]={1,-1,1,-1};

std::vector<std::vector<node> >turn;
std::set<node>tot;

void init()
{
	tot.clear();
	return ;
}

bool check(int x, int y)
{
	return x >= 1 && x <= 6000 && y >= 1 && y <= 6000 && visit[x][y];
}

int main()
{
	turn.resize(10000000);
	for (int i = 0; i <= 6000; i++) 
	{
		for (int j = 0; j <= 6000; j++) 
		{
			if(i*i+j*j <= 10000000)turn[i*i+j*j].push_back((node){i,j});
			else break;
		}
	}

	int t, times=1;
	scanf("%d", &t);
	while(t--)
	{
		init();
		scanf("%d%d", &n, &m);
		int x, y, v, k;
		for (int i = 1; i <= n; i++) 
		{
			scanf("%d%d%d", &x, &y, &v);
			visit[x][y] = true;
			val[x][y] = v;
			tot.insert((node){x, y});
		}
		std::set<node>s;
		int temp;
		long long int last = 0;
		printf("Case #%d:\n", times++);

		for (int i = 1; i <= m; i++) 
		{
			scanf("%d%d%d", &temp, &x, &y);
			x = (x + last) % 6000 + 1;
			y = (y + last) % 6000 + 1;
			s.clear();
			if(temp == 1)
			{
				scanf("%d", &v);
				visit[x][y] = true;
				val[x][y] = v;
				tot.insert((node){x, y});
			}
			else if(temp == 2)
			{
				visit[x][y] = false;
				val[x][y] = 0;
			}
			else if(temp == 3)
			{
				scanf("%d%d", &k, &v);
				for(auto &it : turn[k])
				{
					for (int j =  0; j < 4; j++) 
					{
						int xx = x + x_turn[j]*it.x;
						int yy = y + y_turn[j]*it.y;
						if(check(xx, yy))s.insert((node){xx, yy});
					}
				}
				for(auto &it : s)val[it.x][it.y] += v;
			}
			else if(temp == 4)
			{
				scanf("%d", &k);
				last = 0;
				for(auto &it : turn[k])
				{
					for (int j = 0; j < 4; j++)
					{
						int xx = x + x_turn[j]*it.x;
						int yy = y + y_turn[j]*it.y;
						if(check(xx, yy))s.insert((node){xx, yy});
					}
				}
				for(auto &it : s)last += val[it.x][it.y];
				printf("%lld\n", last);
			}
		}
		for(auto &it : tot)
		{
			visit[it.x][it.y] = false;
			val[it.x][it.y] = 0;
		}
	}
	return 0;
}
