#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int n, mod;

long long int ans;

void DFS(int remain, int last)
{
	if(remain == 0)
	{
		ans++;
		ans %= mod;
		return ;
	}
	for(int i=std::min(remain, last); i>=1; i--)
		DFS(remain-i, i);
	return ;
}

int main()
{
	read(n);
	read(mod);
	DFS(n, 0x7f7f7f7f);
	printf("%lld",ans);
	return 0;
}
