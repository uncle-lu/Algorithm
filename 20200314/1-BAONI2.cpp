#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int n, mod;

long long int F[10010];

int main()
{
	read(n);read(mod);
	for(int i=1; i<=n; ++i)
	{
		for(int j=1;j<i/2;++j)
		{
			F[i] += F[j];
			F[i] %= mod;
		}
		F[i]++;
	}

	printf("%lld",F[n]);

	return 0;
}
