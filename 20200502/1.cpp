#include<cstdio>//uncle-lu
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int N = 5 * 1e5 + 10;

int line[N], tree[N];
int n, m;

int lowbit(int x)
{
	return x & (-x);
}

void update(int i, int k)
{
	while( i <= n )
	{
		tree[i] += k;
		i += lowbit(i);
	}
	return ;
}

int getsum(int i)
{
	int sum = 0;
	while(i > 0)
	{
		sum += tree[i];
		i -= lowbit(i);
	}
	return sum;
}

int main()
{
	read(n);read(m);
	for (int i = 1; i <= n; i++) 
	{
		read(line[i]);
		update(i, line[i]);
	}

	int temp, x, y;
	for (int i = 1; i <= m; i++) 
	{
		read(temp);
		if(temp == 1)
		{
			read(x);read(y);
			update(x, y);
		}
		if(temp == 2)
		{
			read(x);read(y);
			printf("%d\n",getsum(y) - getsum(x-1));
		}
	}
	return 0;
}
