#include<cstdio>//uncle-lu
#include<cmath>
#include<cstring>

bool visit[5];
double line[5];

bool DFS(int step)
{
	if(step == 4)
	{
		for(int i=1;i<=4;++i)
		{
			if(!visit[i])
			{
				if(fabs(line[i] - 24.0) < 1e-6)
					return true;
				else
					return false;
			}
		}

		return false;
	}

	for(int i=1;i<=4;++i)
	{
		if(!visit[i])
		{
			for(int j=1; j<=4; ++j)
			{
				if(!visit[j] && i != j)
				{
					visit[j] = true;
					double temp1 = line[i], temp2 = line[j];

					line[i] = temp1 - temp2;
					if(DFS(step+1))return true;
					line[i] = temp1 + temp2;
					if(DFS(step+1))return true;
					line[i] = temp1 * temp2;
					if(DFS(step+1))return true;
					line[i] = temp1 / temp2;
					if(DFS(step+1))return true;

					visit[j] = false;
					line[i] = temp1;
				}
			}
		}
	}

	return false;
}

int main()
{
	while(true)
	{
		scanf("%lf %lf %lf %lf", &line[1], &line[2], &line[3], &line[4]);
		if(line[1] == 0 && line[2] == 0 && line[3] == 0 && line[4] == 0)break;

		memset(visit,false,sizeof(visit));
		if(DFS(1))
			printf("YES\n");
		else
			printf("NO\n");
	}

	return 0;
}
