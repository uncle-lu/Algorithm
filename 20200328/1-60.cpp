#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

void BFS();

const int N = 100010;
int l[N], r[N], line[N], list[N];
int F[N];
int n;

bool visit[N];
int stack[N];
void BFS()
{
	int now = 1, sit = 1, top = 1;
	stack[top] = now;
	while(top)
	{
		now = stack[top];
		if(l[now] && !visit[ l[now] ])
		{
			stack[++top] = l[now];
			continue;
		}

		list[sit] = line[now] - sit; sit++; top--;
		visit[now] = true;

		if(r[now] && !visit[ r[now] ])
		{
			stack[++top] = r[now];
			continue;
		}
	}
	return ;
}

int main()
{
	read(n);
	for (int i = 1; i <= n; i++)
	{
		read(line[i]);
	}
	int fa, temp;
	for (int i = 2; i <= n; i++)
	{
		read(fa);read(temp);
		(temp ? r[fa]:l[fa]) = i;
	}

	BFS();

	int ans = 0;
	for (int i = 1; i <= n; i++) F[i] = 1;

	for (int i = 1; i <= n; i++) 
	{
		for (int j = 1; j < i; j++) 
		{
			if(list[i] >= list[j])
				F[i] = std::max(F[i], F[j]+1);
		}
	}

	for (int i = 1; i <= n; i++)
	{
		ans = std::max(ans, F[i]);
	}

	printf("%d", n-ans);

	return 0;
}
