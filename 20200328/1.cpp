#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int N = 1e5 + 10;
int line[N], l[N], r[N], list[N], F[N];
int n;

int stack[N], top, now, sit;
bool visit[N];
void BFS()
{
	sit = 1; top = 1; now = 1; stack[top] = 1;
	while(top)
	{
		now = stack[top];
		if(l[now] && !visit[ l[now] ])
		{
			stack[++top] = l[now];
			continue;
		}

		list[sit] = line[now] - sit;
		sit++; top--;
		visit[now] = true;

		if(r[now] && !visit[ r[now] ])
		{
			stack[++top] = r[now];
			continue;
		}
	}
	return ;
}

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
	{
		read(line[i]);
	}

	int fa, temp;
	for (int i = 2; i <= n; i++) 
	{
		read(fa);read(temp);
		(temp ? r[fa] : l[fa]) = i;
	}

	BFS();

	int len = 1;
	F[1] = list[1];
	for (int i = 2; i <= n; i++) 
	{
		if(F[len] <= list[i])
			F[++len] = list[i];
		else
		{
			int left = 1, right = len, mid;
			while(left <= right)
			{
				mid = ( left + right ) >> 1;
				if(list[i] >= F[mid])
					left = mid + 1;
				else
					right = mid - 1;
			}
			F[left] = list[i];
		}
	}

	printf("%d", n - len );

	return 0;
}
