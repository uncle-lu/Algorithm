#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
using namespace std;

const int N = 1e5 + 3;
int n, fa, d, sum, qr, l, r, mid, top, stk[N], f[N], a[N], b[N], lc[N], rc[N];
bool vis[N];

char ch;
int read() {
	while (ch = getchar(), ch < '0' || ch > '9');
	int res = ch - 48;
	while (ch = getchar(), ch >= '0' && ch <= '9') res = res * 10 + ch - 48;
	return res;
}

void Bfs() {
	int x; stk[top = 1] = 1;
	while (top) {
		x = stk[top];
		if (lc[x] && !vis[lc[x]]) {
			stk[++top] = lc[x];
			continue;
		}
		b[++sum] = a[x]; b[sum] -= sum;
		vis[x] = true; --top;
		if (rc[x] && !vis[rc[x]]) {
			stk[++top] = rc[x];
			continue;
		}
	}
	return ;
}

int main() {
	//freopen("binary.in", "r", stdin);
	//freopen("binary.out", "w", stdout);
	n = read();
	for (int i = 1; i <= n; ++i) a[i] = read();
	for (int i = 2; i <= n; ++i) {
		fa = read(); d = read();
		(d ? rc[fa] : lc[fa]) = i;
	}
	Bfs();
	for (int i = 1; i <= n; i++) {
		printf("%d ", b[i]);
	}
	f[qr = 1] = b[1];
	for (int i = 2; i <= n; ++i) {
		if (b[i] >= f[qr]) f[++qr] = b[i];
		else {
			l = 1; r = qr;
			while (l <= r) {
				mid = (l + r) >> 1;
				if (f[mid] <= b[i]) l = mid + 1;
				else 	r = mid - 1;
			}
			f[l] = b[i];
		}
	}
	cout << n - qr << endl;
	//fclose(stdin); fclose(stdout);
	return 0;
}
